/*
Navicat MySQL Data Transfer

Source Server         : Mysql
Source Server Version : 50618
Source Host           : localhost:3306
Source Database       : fhbootv

Target Server Type    : MYSQL
Target Server Version : 50618
File Encoding         : 65001

Date: 2019-05-26 23:36:56
*/

create database fm;
use fm;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_fhdb`
-- ----------------------------
DROP TABLE IF EXISTS `DB_FHDB`;
CREATE TABLE `DB_FHDB` (
  `FHDB_ID` varchar(100) NOT NULL,
  `USERNAME` varchar(50) DEFAULT NULL COMMENT '操作用户',
  `BACKUP_TIME` varchar(32) DEFAULT NULL COMMENT '备份时间',
  `TABLENAME` varchar(50) DEFAULT NULL COMMENT '表名',
  `SQLPATH` varchar(300) DEFAULT NULL COMMENT '存储位置',
  `TYPE` int(1) NOT NULL COMMENT '类型',
  `DBSIZE` varchar(10) DEFAULT NULL COMMENT '文件大小',
  `BZ` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`FHDB_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_fhdb
-- ----------------------------

-- ----------------------------
-- Table structure for `DB_TIMINGBACKUP`
-- ----------------------------
DROP TABLE IF EXISTS `DB_TIMINGBACKUP`;
CREATE TABLE `db_timingbackup` (
  `TIMINGBACKUP_ID` varchar(100) NOT NULL,
  `JOBNAME` varchar(50) DEFAULT NULL COMMENT '任务名称',
  `CREATE_TIME` varchar(32) DEFAULT NULL COMMENT '创建时间',
  `TABLENAME` varchar(50) DEFAULT NULL COMMENT '表名',
  `STATUS` int(1) NOT NULL COMMENT '类型',
  `FHTIME` varchar(30) DEFAULT NULL COMMENT '时间规则',
  `TIMEEXPLAIN` varchar(100) DEFAULT NULL COMMENT '规则说明',
  `BZ` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`TIMINGBACKUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_timingbackup
-- ----------------------------
INSERT INTO `DB_TIMINGBACKUP` VALUES ('e3b20e0a9ca9457fadb46e87668c67b9', 'act_ge_bytearray_998372', '2019-05-23 12:00:57', 'act_ge_bytearray', '2', '0 0 * ? 3 TUES', '每年 3 月份的 每个星期二  每小时执行一次', 'fff');

-- ----------------------------
-- Table structure for `im_fgroup`
-- ----------------------------
DROP TABLE IF EXISTS `IM_FGROUP`;
CREATE TABLE `IM_FGROUP` (
  `FGROUP_ID` varchar(100) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL COMMENT '组名',
  `USERNAME` varchar(100) DEFAULT NULL COMMENT '用户名',
  PRIMARY KEY (`FGROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of im_fgroup
-- ----------------------------
INSERT INTO `IM_FGROUP` VALUES ('1e37e06c56f346b497b39f1e0471f0e4', '朋友', 'zhangsan');
INSERT INTO `IM_FGROUP` VALUES ('455661b624e148f9960fa1630e04a9f0', '同事', 'lisi');
INSERT INTO `IM_FGROUP` VALUES ('76c1b136283040888763ce3e663588ca', '同事', 'zhangsan');
INSERT INTO `IM_FGROUP` VALUES ('7f89eaecacac40e19678db39c47664d3', '同事', 'admin');
INSERT INTO `IM_FGROUP` VALUES ('8db01045031e4f1fb4dae69d820ca2b3', '朋友', 'admin');

-- ----------------------------
-- Table structure for `IM_FRIENDS`
-- ----------------------------
DROP TABLE IF EXISTS `IM_FRIENDS`;
CREATE TABLE `im_friends` (
  `FRIENDS_ID` varchar(100) NOT NULL,
  `USERNAME` varchar(100) DEFAULT NULL COMMENT '用户名',
  `FUSERNAME` varchar(100) DEFAULT NULL COMMENT '好友用户名',
  `CTIME` varchar(32) DEFAULT NULL COMMENT '添加时间',
  `ALLOW` varchar(10) DEFAULT NULL COMMENT '是否允许',
  `FGROUP_ID` varchar(100) DEFAULT NULL,
  `DTIME` varchar(32) DEFAULT NULL,
  `BZ` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`FRIENDS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of im_friends
-- ----------------------------
INSERT INTO `IM_FRIENDS` VALUES ('02a207bfe9a047d2b5ecdbb0c1738472', 'lisi', 'zhangsan', '2019-05-14 18:49:44', 'yes', '455661b624e148f9960fa1630e04a9f0', '2019-05-14 18:49:44', '同意对方申请好友并加对方好友');
INSERT INTO `IM_FRIENDS` VALUES ('2c5da96b18cb4771b97ba7be3772b8dd', 'admin', 'zhangsan', '2019-05-23 03:20:54', 'yes', '7f89eaecacac40e19678db39c47664d3', '2019-05-23 03:20:54', '同意对方申请好友并加对方好友');
INSERT INTO `IM_FRIENDS` VALUES ('7862f28920a04a2c8094e874567c820a', 'zhangsan', 'admin', '2019-05-23 03:20:47', 'yes', '76c1b136283040888763ce3e663588ca', '2019-05-23 03:20:54', '申请加好友');

-- ----------------------------
-- Table structure for `im_hismsg`
-- ----------------------------
DROP TABLE IF EXISTS `IM_HISMSG`;
CREATE TABLE `IM_HISMSG` (
  `HISMSG_ID` varchar(100) NOT NULL,
  `USERNAME` varchar(100) DEFAULT NULL COMMENT '发送者',
  `TOID` varchar(100) DEFAULT NULL COMMENT '目标(好友或者群)',
  `TYPE` varchar(30) DEFAULT NULL COMMENT '类型',
  `NAME` varchar(100) DEFAULT NULL COMMENT '发送者姓名',
  `PHOTO` varchar(255) DEFAULT NULL COMMENT '发送者头像',
  `CTIME` varchar(100) DEFAULT NULL COMMENT '发送时间',
  `CONTENT` longtext COMMENT '发送消息内容',
  `DREAD` varchar(10) DEFAULT NULL COMMENT '是否已读',
  `OWNER` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`HISMSG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of im_hismsg
-- ----------------------------
INSERT INTO `IM_HISMSG` VALUES ('44912231c1884f01b56ee3e9d91b4967', 'zhangsan', 'admin', 'friend', '张三', 'assets/images/user/avatar-2.jpg', '2019-05-26 01:43:57', '333', '1', 'zhangsan');
INSERT INTO `IM_HISMSG` VALUES ('6a968af1550e4df59ee9af187f970e6f', 'admin', 'zhangsan', 'friend', '系统管理员', 'assets/images/user/avatar-2.jpg', '2019-05-26 01:35:57', 'img[uploadFiles/imgs/20190526/dbe8b21291304e8a947de0823e58cf74.jpg]', '1', 'zhangsan');
INSERT INTO `IM_HISMSG` VALUES ('7db2f7daaa4743ddb93bb31e42efa853', 'admin', 'zhangsan', 'friend', '系统管理员', 'assets/images/user/avatar-2.jpg', '2019-05-26 01:36:04', 'file(uploadFiles/file/20190526/9e84afa065534b09b246dec3c883501f.rar)[9e84afa065534b09b246dec3c883501f.rar]', '1', 'zhangsan');
INSERT INTO `IM_HISMSG` VALUES ('e7d9794d607749caabc3ff80e44c625d', 'admin', 'zhangsan', 'friend', '系统管理员', 'assets/images/user/avatar-2.jpg', '2019-05-26 01:35:24', '你好', '1', 'zhangsan');
INSERT INTO `IM_HISMSG` VALUES ('ff85823929464d6385aba18ef0bfcf69', 'admin', 'zhangsan', 'friend', '系统管理员', 'assets/images/user/avatar-2.jpg', '2019-05-26 01:35:44', 'face[礼物] ', '1', 'zhangsan');

-- ----------------------------
-- Table structure for `im_imstate`
-- ----------------------------
DROP TABLE IF EXISTS `IM_IMSTATE`;
CREATE TABLE `IM_IMSTATE` (
  `IMSTATE_ID` varchar(100) NOT NULL,
  `USERNAME` varchar(100) DEFAULT NULL COMMENT '用户名',
  `ONLINE` varchar(10) DEFAULT NULL COMMENT '在线',
  `AUTOGRAPH` varchar(255) DEFAULT NULL COMMENT '个性签名',
  `SIGN` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`IMSTATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of im_imstate
-- ----------------------------
INSERT INTO `IM_IMSTATE` VALUES ('49548c35352d4337b4978a198bdce889', 'admin', 'online', 'I LOVE FH Admin', '4.jpg');
INSERT INTO `IM_IMSTATE` VALUES ('69ffe5039fbb4735aa5e163efcd4070b', 'zhangsan', 'online', 'I LOVE FH Admin', '5.jpg');

-- ----------------------------
-- Table structure for `im_iqgroup`
-- ----------------------------
DROP TABLE IF EXISTS `IM_IQGROUP`;
CREATE TABLE `IM_IQGROUP` (
  `IQGROUP_ID` varchar(100) NOT NULL,
  `USERNAME` varchar(100) DEFAULT NULL COMMENT '用户名',
  `QGROUPS` longtext COMMENT '群的ID组合',
  PRIMARY KEY (`IQGROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of im_iqgroup
-- ----------------------------
INSERT INTO `IM_IQGROUP` VALUES ('16204d8eeeb94e63abe41486d59e0865', 'admin', '(\'09c38d77167c48e3a96821bf97d4cac9\',');

-- ----------------------------
-- Table structure for `im_qgroup`
-- ----------------------------
DROP TABLE IF EXISTS `IM_QGROUP`;
CREATE TABLE `IM_QGROUP` (
  `QGROUP_ID` varchar(100) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL COMMENT '群名称',
  `USERNAME` varchar(100) DEFAULT NULL COMMENT '群主',
  `PHOTO` varchar(255) DEFAULT NULL COMMENT '群头像',
  `CTIME` varchar(32) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`QGROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of im_qgroup
-- ----------------------------
INSERT INTO `IM_QGROUP` VALUES ('09c38d77167c48e3a96821bf97d4cac9', '同事', 'admin', 'uploadFiles/imgs/20190525/fb7047e8099d4453b9af1589559a53ca.jpg', '2019-05-25 22:32:27');

-- ----------------------------
-- Table structure for `IM_SYSMSG`
-- ----------------------------
DROP TABLE IF EXISTS `IM_SYSMSG`;
CREATE TABLE `im_sysmsg` (
  `SYSMSG_ID` varchar(100) NOT NULL,
  `USERNAME` varchar(100) DEFAULT NULL COMMENT '接收者用户名',
  `FROMUSERNAME` varchar(100) DEFAULT NULL COMMENT '发送者用户名',
  `CTIME` varchar(32) DEFAULT NULL COMMENT '操作时间',
  `REMARK` varchar(255) DEFAULT NULL COMMENT '留言',
  `TYPE` varchar(50) DEFAULT NULL COMMENT '类型',
  `CONTENT` varchar(100) DEFAULT NULL COMMENT '事件内容',
  `ISDONE` varchar(30) DEFAULT NULL COMMENT '是否完成',
  `DTIME` varchar(32) DEFAULT NULL COMMENT '完成时间',
  `QGROUP_ID` varchar(100) DEFAULT NULL,
  `DREAD` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`SYSMSG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of im_sysmsg
-- ----------------------------

-- ----------------------------
-- Table structure for `SYS_CODEEDITOR`
-- ----------------------------
DROP TABLE IF EXISTS `SYS_CODEEDITOR`;
CREATE TABLE `sys_codeeditor` (
  `CODEEDITOR_ID` varchar(100) NOT NULL,
  `TYPE` varchar(30) DEFAULT NULL COMMENT '类型',
  `FTLNMAME` varchar(30) DEFAULT NULL COMMENT '文件名',
  `CTIME` varchar(32) DEFAULT NULL COMMENT '创建时间',
  `CODECONTENT` text COMMENT '代码',
  PRIMARY KEY (`CODEEDITOR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_codeeditor
-- ----------------------------

-- ----------------------------
-- Table structure for `SYS_CREATECODE`
-- ----------------------------
DROP TABLE IF EXISTS `SYS_CREATECODE`;
CREATE TABLE `sys_createcode` (
  `CREATECODE_ID` varchar(100) NOT NULL,
  `PACKAGENAME` varchar(50) DEFAULT NULL COMMENT '包名',
  `OBJECTNAME` varchar(50) DEFAULT NULL COMMENT '类名',
  `TABLENAME` varchar(50) DEFAULT NULL COMMENT '表名',
  `FIELDLIST` longtext COMMENT '属性集',
  `CREATETIME` varchar(100) DEFAULT NULL COMMENT '创建时间',
  `TITLE` varchar(255) DEFAULT NULL COMMENT '描述',
  `FHTYPE` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`CREATECODE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_createcode
-- ----------------------------
INSERT INTO `SYS_CREATECODE` VALUES ('0fdfa1b9f56f4c8492fc5a0423d82faa', 'stu', 'Student', 'TB_,fh,STUDENT', 'SNAME,fh,String,fh,姓名,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790SEX,fh,String,fh,性别,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790DDS,fh,String,fh,数据字典,fh,是,fh,无,fh,100,fh,0,fh,1Q313596790AGE,fh,Integer,fh,年龄,fh,是,fh,无,fh,3,fh,0,fh,nullQ313596790BIRITHDAY,fh,Date,fh,生日,fh,是,fh,无,fh,32,fh,0,fh,nullQ313596790HSS,fh,String,fh,字典2,fh,是,fh,无,fh,100,fh,0,fh,6d30b170d4e348e585f113d14a4dd96dQ313596790', '2019-05-14 20:31:46', '学生信息', 'fathertable');
INSERT INTO `SYS_CREATECODE` VALUES ('15acceab3cc346bbaa9dabf18e45487c', 'stu', 'Student', 'TB_,fh,STUDENT', 'SNAME,fh,String,fh,姓名,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790SEX,fh,String,fh,性别,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790DDS,fh,String,fh,数据字典,fh,是,fh,无,fh,100,fh,0,fh,1Q313596790AGE,fh,Integer,fh,年龄,fh,是,fh,无,fh,3,fh,0,fh,nullQ313596790BIRITHDAY,fh,Date,fh,生日,fh,是,fh,无,fh,32,fh,0,fh,nullQ313596790HSS,fh,String,fh,字典2,fh,是,fh,无,fh,100,fh,0,fh,6d30b170d4e348e585f113d14a4dd96dQ313596790', '2019-05-13 04:50:16', '学生信息', 'tree');
INSERT INTO `SYS_CREATECODE` VALUES ('2fa0aeb35fa748469f00f8a8ef71469b', 'stu', 'Student', 'TB_,fh,STUDENT', 'SNAME,fh,String,fh,姓名,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790SEX,fh,String,fh,性别,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790DDS,fh,String,fh,数据字典,fh,是,fh,无,fh,100,fh,0,fh,1Q313596790AGE,fh,Integer,fh,年龄,fh,是,fh,无,fh,3,fh,0,fh,nullQ313596790BIRITHDAY,fh,Date,fh,生日,fh,是,fh,无,fh,32,fh,0,fh,nullQ313596790HSS,fh,String,fh,字典2,fh,是,fh,无,fh,100,fh,0,fh,6d30b170d4e348e585f113d14a4dd96dQ313596790', '2019-05-13 04:49:27', '学生信息', 'fathertable');
INSERT INTO `SYS_CREATECODE` VALUES ('52ef5b4d679c448f87587e6e5841145e', 'stu', 'StudentMx', 'TB_,fh,STUDENTMX', 'SNAME,fh,String,fh,姓名,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790SEX,fh,String,fh,性别,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790DDS,fh,String,fh,数据字典,fh,是,fh,无,fh,100,fh,0,fh,1Q313596790AGE,fh,Integer,fh,年龄,fh,是,fh,无,fh,3,fh,0,fh,nullQ313596790BIRITHDAY,fh,Date,fh,生日,fh,是,fh,无,fh,32,fh,0,fh,nullQ313596790HSS,fh,String,fh,字典2,fh,是,fh,无,fh,100,fh,0,fh,6d30b170d4e348e585f113d14a4dd96dQ313596790', '2019-05-13 04:34:49', '学生信息(明细)', 'sontable');
INSERT INTO `SYS_CREATECODE` VALUES ('627930ba41e04e8b9dfd719ecb919cd4', 'stu', 'Student', 'TB_,fh,STUDENT', 'SNAME,fh,String,fh,姓名,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790SEX,fh,String,fh,性别,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790DDS,fh,String,fh,数据字典,fh,是,fh,无,fh,100,fh,0,fh,1Q313596790AGE,fh,Integer,fh,年龄,fh,是,fh,无,fh,3,fh,0,fh,nullQ313596790BIRITHDAY,fh,Date,fh,生日,fh,是,fh,无,fh,32,fh,0,fh,nullQ313596790HSS,fh,String,fh,字典2,fh,是,fh,无,fh,100,fh,0,fh,6d30b170d4e348e585f113d14a4dd96dQ313596790', '2019-05-25 03:44:45', '学生信息', 'single');
INSERT INTO `SYS_CREATECODE` VALUES ('b5380bb4940e48e99f6743b8d9b874d4', 'stu', 'Student', 'TB_,fh,STUDENT', 'SNAME,fh,String,fh,姓名,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790SEX,fh,String,fh,性别,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790DDS,fh,String,fh,数据字典,fh,是,fh,无,fh,100,fh,0,fh,1Q313596790AGE,fh,Integer,fh,年龄,fh,是,fh,无,fh,3,fh,0,fh,nullQ313596790BIRITHDAY,fh,Date,fh,生日,fh,是,fh,无,fh,32,fh,0,fh,nullQ313596790HSS,fh,String,fh,字典2,fh,是,fh,无,fh,100,fh,0,fh,6d30b170d4e348e585f113d14a4dd96dQ313596790', '2019-05-25 22:27:24', '学生信息', 'single');
INSERT INTO `SYS_CREATECODE` VALUES ('c2fa81b3709f4317a0bc694f755f317d', 'stu', 'Student', 'TB_,fh,STUDENT', 'SNAME,fh,String,fh,姓名,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790SEX,fh,String,fh,性别,fh,是,fh,无,fh,255,fh,0,fh,nullQ313596790DDS,fh,String,fh,数据字典,fh,是,fh,无,fh,100,fh,0,fh,1Q313596790AGE,fh,Integer,fh,年龄,fh,是,fh,无,fh,3,fh,0,fh,nullQ313596790BIRITHDAY,fh,Date,fh,生日,fh,是,fh,无,fh,32,fh,0,fh,nullQ313596790HSS,fh,String,fh,字典2,fh,是,fh,无,fh,100,fh,0,fh,6d30b170d4e348e585f113d14a4dd96dQ313596790', '2019-05-25 03:42:50', '学生信息', 'tree');

-- ----------------------------
-- Table structure for `SYS_DICTIONARIES`
-- ----------------------------
DROP TABLE IF EXISTS `SYS_DICTIONARIES`;
CREATE TABLE `SYS_DICTIONARIES` (
  `DICTIONARIES_ID` varchar(100) NOT NULL,
  `NAME` varchar(30) DEFAULT NULL COMMENT '名称',
  `NAME_EN` varchar(50) DEFAULT NULL COMMENT '英文',
  `BIANMA` varchar(50) DEFAULT NULL COMMENT '编码',
  `ORDER_BY` int(11) NOT NULL COMMENT '排序',
  `PARENT_ID` varchar(100) DEFAULT NULL COMMENT '上级ID',
  `BZ` varchar(255) DEFAULT NULL COMMENT '备注',
  `TBSNAME` varchar(100) DEFAULT NULL COMMENT '排查表',
  `TBFIELD` varchar(100) DEFAULT NULL,
  `YNDEL` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`DICTIONARIES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_DICTIONARIES
-- ----------------------------
INSERT INTO `SYS_DICTIONARIES` VALUES ('00ef925d227444859eef2057693722ae', '达州', 'dazhou', '0032504', '4', 'd3538add7125404aba4b0007ef9fde50', '达州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('010692054ac24eeebf5b8067f0f0521a', '安庆', 'anqing', '0030401', '1', '249999f296d14f95b8138a30bbb2c374', '安庆市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('023473e9e6204583a110531036357514', '山西', 'shanxi', '00323', '23', '1', '山西省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('02e5eb8f50bd4824ad97427e2b372d14', '绥化', 'suihua', '0031312', '12', 'b2d4133b5dbf4599ada940620d2ab250', '绥化市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('035fe989f54742ac8b64b80b24213442', '来宾', 'laibin', '0030809', '9', 'c5f3d426c582410281f89f1451e1d854', '来宾市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('055273fe79f94e09a64698dab8d30ea8', '揭阳', 'jieyang', '0030708', '8', '0dd1f40bcb9d46aeba015dc19645a5b9', '揭阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('05ab2921b64d4f5c935c35228cc49ecb', '大同', 'datong', '0032302', '2', '023473e9e6204583a110531036357514', '大同市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('05ed855479d94b139c77ae82452bb39c', '涪陵区', 'fulingqu', '0033102', '2', '1c85fbd06cf840d093f3640aca1b6b2d', '涪陵区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('076995f7d0034b32a94e0130d406d137', '湖州', 'huzhou', '0033002', '2', '6d846178376549ed878d11d109819f25', '湖州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('076a163af6814f93954a543bd3b2fa4d', '广州', 'guangzhou', '0030704', '4', '0dd1f40bcb9d46aeba015dc19645a5b9', '广州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('085ebd2776384eff842de8b61b781a7e', '潼南区', 'tongnanqu', '0033122', '22', '1c85fbd06cf840d093f3640aca1b6b2d', '潼南区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('0953fe05e34642169c4cf24492b163b9', '湘西', 'xiangxi', '0031514', '14', 'c59f91630bef4289b71fcb2a48994582', '湘西市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('098bf5e3603e44889a2c4bb25e350400', '阿坝', 'a\'ba', '0032501', '1', 'd3538add7125404aba4b0007ef9fde50', '阿坝市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('0a2561ec256b4f46b4fa76c621256595', '鹤岗', 'hegang', '0031304', '4', 'b2d4133b5dbf4599ada940620d2ab250', '鹤岗市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('0a65f2ef68d54b7c8772e1d916684c4a', '岳阳', 'yueyang', '0031506', '6', 'c59f91630bef4289b71fcb2a48994582', '岳阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('0a754e878c304b99bf5d34a82ca3705c', '吉林', 'jilin', '0031604', '4', '857be71b0d6d4a40a2c83476824206d1', '吉林市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('0b08e52f2b264d0da66d37e718e32aba', '常德', 'changde', '0031507', '7', 'c59f91630bef4289b71fcb2a48994582', '常德市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('0c908137935946ac885cb56e55ff4f5d', '北碚区', 'beibeiqu', '0033109', '9', '1c85fbd06cf840d093f3640aca1b6b2d', '北碚区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('0dba32de24014bcab807fd0fc51953aa', '北海', 'beihai', '0030802', '2', 'c5f3d426c582410281f89f1451e1d854', '北海市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('0dd1f40bcb9d46aeba015dc19645a5b9', '广东', 'guangdong', '00307', '7', '1', '广东省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('0e18ab3edf5e43ee8737c156b0b50692', '吉安', 'ji\'an', '0031803', '3', 'cb3008cbd6ae4b5f8cebd2254ccb8603', '吉安市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('0eb279a28a0d43c7a075d58c6cfc3e02', '长寿区', 'changshouqu', '0033115', '15', '1c85fbd06cf840d093f3640aca1b6b2d', '长寿区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('0efda23d751b42cb9472ca4f80cdf6c5', '德州', 'dezhou', '0030303', '3', '10f46a521ea0471f8d71ee75ac3b5f3a', '德州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('0f975f2f51e245439b7d759f822a4a43', '嘉定区', 'jiadingqu', '0030210', '10', 'f1ea30ddef1340609c35c88fb2919bee', '嘉定区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1', '地区', 'area', '003', '1', '0', '地区', '', '', null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('10a2b2b54bce432baf603c7fa4b45de0', '淮北', 'huaibei', '0030409', '9', '249999f296d14f95b8138a30bbb2c374', '淮北市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('10c14cd82df9496bb86c5681ddfb92fb', '本溪', 'benxi', '0031902', '2', 'b3366626c66c4b61881f09e1722e8495', '本溪市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('10f46a521ea0471f8d71ee75ac3b5f3a', '山东', 'shandong', '00303', '3', '1', '山东省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('10f5278b19824877988e8baa5a1b58f4', '邯郸', 'handan', '0031104', '4', '75362368f22f4d60a810c2a45cced487', '邯郸市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('11ab8df614c14451bb08a91fbe05162e', '防城港', 'fangchenggang', '0030804', '4', 'c5f3d426c582410281f89f1451e1d854', '防城港市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('12a62a3e5bed44bba0412b7e6b733c93', '北京', 'beijing', '00301', '1', '1', '北京', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('13b4d440cdd043378c2bbd0b797bc7b7', '黄石', 'huangshi', '0031404', '4', '312b80775e104ba08c8244a042a658df', '黄石市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('13e9e380abed4def837bea1671b92633', '大兴安岭', 'daxinganling', '0031302', '2', 'b2d4133b5dbf4599ada940620d2ab250', '大兴安岭市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('14452abafbef4cadbb05a5a74a61eb6f', '广安', 'guang\'an', '0032507', '7', 'd3538add7125404aba4b0007ef9fde50', '广安市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('158588bf48464dcca0e656968b8e09c0', '密云区', 'miyunqu', '0030115', '15', '12a62a3e5bed44bba0412b7e6b733c93', '密云区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('159d49075827476490aee58956fb159c', '潜江', 'qianjiang', '0031406', '6', '312b80775e104ba08c8244a042a658df', '潜江市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('15da226f465b4dac95c8333fd3d81747', '淮安', 'huaian', '0031702', '2', '577405ff648240959b3765c950598ab0', '淮安市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('16535e38a2534f4781353e7570831ea1', '丽江', 'lijiang', '0032909', '9', '510607a1836e4079b3103e14ec5864ed', '丽江', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('165fd1c02d98439d8d7cc2e81def88d6', '陇南', 'longnan', '0030609', '9', '3283f1a77180495f9a0b192d0f9cdd35', '陇南市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('16a1eb63489e4d28827fc16a90e2ed61', '贵港', 'guigang', '0030805', '5', 'c5f3d426c582410281f89f1451e1d854', '贵港市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1895a514cda74329817bce6a5fe918f4', '济源', 'jiyuan', '0031203', '3', '7336944efb4b40fcae9118fc9a970d2d', '济源市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1929f99821f2484fa33991233c1555e9', '大理', 'dali', '0032904', '4', '510607a1836e4079b3103e14ec5864ed', '大理', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('192a36eb3d234a909e339c06b9cf723a', '许昌', 'xuchang', '0031215', '15', '7336944efb4b40fcae9118fc9a970d2d', '许昌市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('19cf8222eac9457280ebb40f14052590', '南岸区', 'nan\'anqu', '0033108', '8', '1c85fbd06cf840d093f3640aca1b6b2d', '南岸区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1a99e7e302ce4f24b4b5d5d4b20a75fe', '清远', 'qingyuan', '0030711', '11', '0dd1f40bcb9d46aeba015dc19645a5b9', '清远市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1ac809034f3d471592a5c74e19c7f1bc', '烟台', 'yantai', '0030315', '15', '10f46a521ea0471f8d71ee75ac3b5f3a', '烟台市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1b65dd2a2057489c9598c789b4114d24', '孝感', 'xiaogan', '0031415', '15', '312b80775e104ba08c8244a042a658df', '孝感市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1b8b44f0c4384a09987ab0c5d9ceedd2', '神农架林区', 'shennongjialinqu', '0031407', '7', '312b80775e104ba08c8244a042a658df', '神农架林区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1c4313525bdc4d4b9f7849dfb614cfb3', '柳州', 'liuzhou', '0030810', '10', 'c5f3d426c582410281f89f1451e1d854', '柳州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1c85fbd06cf840d093f3640aca1b6b2d', '重庆', 'chongqing', '00331', '31', '1', '重庆', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1cf92384c7ee46faa91acface462b32f', '沧州', 'cangzhou', '0031102', '2', '75362368f22f4d60a810c2a45cced487', '沧州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1d0deff7da2745cc960cfa9ae07bdd13', '抚顺', 'fushun', '0031906', '6', 'b3366626c66c4b61881f09e1722e8495', '抚顺市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1d6d2d9af05849da9807d4cba0144695', '南通', 'nantong', '0031705', '5', '577405ff648240959b3765c950598ab0', '南通市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1e228d052ec24bb7ba64524f51689cef', '鄂尔多斯', 'eerduosi', '0032005', '5', 'c072c248c7ab47dda7bf24f5e577925c', '鄂尔多斯市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1e429ce404794a30aad09bc592d0f5ce', '荆门', 'jingmen', '0031405', '5', '312b80775e104ba08c8244a042a658df', '荆门市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('1e89ca839dbf46a3bc8c02b7d55802c5', '长沙', 'changsha', '0031501', '1', 'c59f91630bef4289b71fcb2a48994582', '长沙市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2087851693514e3c9c98fd843fb5b32c', '河西区', 'hexiqu', '0032603', '3', '2c254799d3454f2cbc338ef5712548e9', '河西区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('20a08ca32856488dad122529f901fb9b', '固原', 'guyuan', '0032101', '1', '5690b0534fe745e5ba0f504f0c260559', '固原市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('20f6d6c3b3234c21b52755ab6b690ffe', '杭州', 'hangzhou', '0033001', '1', '6d846178376549ed878d11d109819f25', '杭州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('212dbe5474304ad8b5f6e6049a72da46', '包头', 'baotou', '0032003', '3', 'c072c248c7ab47dda7bf24f5e577925c', '包头市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('217c993dce9544c89279e88bdd60e7a8', '黄冈', 'huanggang', '0031403', '3', '312b80775e104ba08c8244a042a658df', '黄冈市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('22ef24142b0a4d6e9f05582e3c8790a9', '济南', 'jinan', '0030301', '1', '10f46a521ea0471f8d71ee75ac3b5f3a', '济南市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2303cab27d704a259d7b0c42a687f3db', '马鞍山', 'maanshan', '0030413', '13', '249999f296d14f95b8138a30bbb2c374', '马鞍山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('23aff8da2e6c4513be3155f372c45046', '石嘴山', 'shizuishan', '0032103', '3', '5690b0534fe745e5ba0f504f0c260559', '石嘴山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('23caa037fe8c4283b7a7fc42da4a45a9', '武威', 'wuwei', '0030613', '13', '3283f1a77180495f9a0b192d0f9cdd35', '武威市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('23f71d660bb94d239dde6738b73d3905', '延边', 'yanbian', '0031609', '9', '857be71b0d6d4a40a2c83476824206d1', '延边市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('24007511f8ec42da8c6555305afe56ce', '青岛', 'qindao', '0030310', '10', '10f46a521ea0471f8d71ee75ac3b5f3a', '青岛市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2431e2f471624fd9bf0f76b7049b1296', '驻马店', 'zhumadian', '0031218', '18', '7336944efb4b40fcae9118fc9a970d2d', '驻马店市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('244ffa16c2cd4594af2dfed2f7257d24', '德阳', 'deyang', '0032505', '5', 'd3538add7125404aba4b0007ef9fde50', '德阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2477f3e5e2c94c73844b060d9dc82316', '深圳', 'shenzhen', '0030715', '15', '0dd1f40bcb9d46aeba015dc19645a5b9', '深圳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('249999f296d14f95b8138a30bbb2c374', '安徽', 'anhui', '00304', '4', '1', '安徽省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('25892ce9cba1429fb1b45d4aaeaf3bca', '石家庄', 'shijiazhuang', '0031108', '8', '75362368f22f4d60a810c2a45cced487', '石家庄市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2627d3e9f98a4cdfbe0f59a4c5d3772a', '白银', 'baiyin', '0030601', '1', '3283f1a77180495f9a0b192d0f9cdd35', '白银市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('26a79d023ad7483194241cddf97f3689', '莱芜', 'laiwu', '0030307', '7', '10f46a521ea0471f8d71ee75ac3b5f3a', '莱芜市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('26b093ae7635474d8da8162efe7e4035', '合川区', 'hechuanqu', '0033117', '17', '1c85fbd06cf840d093f3640aca1b6b2d', '合川区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('26d4e79797d34b11b58eb12e5c0c55ae', '抚州', 'fuzhou', '0031801', '1', 'cb3008cbd6ae4b5f8cebd2254ccb8603', '抚州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('273f2c545056473abaf320327073b48b', '无锡', 'wuxi', '0031709', '9', '577405ff648240959b3765c950598ab0', '无锡市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('27927fbc83154894b096221da15b326a', '保定', 'baoding', '0031101', '1', '75362368f22f4d60a810c2a45cced487', '保定市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('287baf1c903444359971b0ce8d58dce2', '普洱', 'pu\'er', '0032912', '12', '510607a1836e4079b3103e14ec5864ed', '普洱', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('29a4dbca082b49078af67caf5fd28f4f', '漳州', 'zhangzhou', '0030509', '9', 'd4066f6f425a4894a77f49f539f2a34f', '漳州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2a4c3b9f024743d19907b36ab4a43499', '宣城', 'xuancheng', '0030417', '17', '249999f296d14f95b8138a30bbb2c374', '宣城市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2a4f0cb0748645bab53b94b62412df04', '黔西南', 'qianxinan', '0030907', '7', '592f6fcf45a74524aa8ea853fc9761d5', '黔西南市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2ae081dd5d3c47b584fdaf24769e49e1', '玉溪', 'yuxi', '0032915', '15', '510607a1836e4079b3103e14ec5864ed', '玉溪', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2ba8e6d0fd944983aa19b781c6b53477', '海南', 'hainan', '00310', '10', '1', '海南省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2bd0431e3566451297ecd194287a878a', '甘孜', 'ganzi', '0032506', '6', 'd3538add7125404aba4b0007ef9fde50', '甘孜', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2c0da3154cc74d7990c597bed6ebf2d6', '大兴区', 'daxingqu', '0030112', '12', '12a62a3e5bed44bba0412b7e6b733c93', '大兴区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2c254799d3454f2cbc338ef5712548e9', '天津', 'tianjin', '00326', '26', '1', '天津', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2d0e4687904b48738ac6bd6a42e7f32d', '忻州', 'xinzhou', '0032309', '9', '023473e9e6204583a110531036357514', '忻州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2d0ff92556b544c19dbfc8b8b055e19a', '常州', 'changzhou', '0031701', '1', '577405ff648240959b3765c950598ab0', '常州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2e3c279cf0a44115869049e4a6d9ed08', '西宁', 'xining', '0032207', '7', '5a80e3435c0e4dc09bafceeadb38e5f0', '西宁', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2f097a8914de4b01a04bf61852435672', '阿勒泰', 'a\'letai', '0032802', '2', '2fabed91c6d94e698ed449165cd250ca', '阿勒泰', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2f5e433682f24e20b600532062ff0bcb', '白山', 'baishan', '0031602', '2', '857be71b0d6d4a40a2c83476824206d1', '白山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2f7f68eb9be845be90e74a0763de2c7f', '平谷区', 'pingguqu', '0030114', '14', '12a62a3e5bed44bba0412b7e6b733c93', '平谷区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2f8e7a55eaab4649b9abe43ade744e58', '大庆', 'daqing', '0031301', '1', 'b2d4133b5dbf4599ada940620d2ab250', '大庆市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('2fabed91c6d94e698ed449165cd250ca', '新疆', 'xinjiang', '00328', '28', '1', '新疆', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('30d424f63bf44e8391683f371ed3552f', '秦皇岛', 'qinhuangdao', '0031107', '7', '75362368f22f4d60a810c2a45cced487', '秦皇岛市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('312b80775e104ba08c8244a042a658df', '湖北', 'hubei', '00314', '14', '1', '湖北省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3186f859efa246f793401c475d3d0090', '阿里', 'a\'li', '0032701', '1', '3e846b08dbbe495e93bc93f8f202de79', '阿里', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('325e45e49c1849efb7fea2296f686210', '赣州', 'ganzhou', '0031802', '2', 'cb3008cbd6ae4b5f8cebd2254ccb8603', '赣州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3283f1a77180495f9a0b192d0f9cdd35', '甘肃', 'gansu', '00306', '6', '1', '甘肃省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('329838f633f340779483910f33387ccd', '通州区', 'tongzhouqu', '0030109', '9', '12a62a3e5bed44bba0412b7e6b733c93', '通州区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3326f321dfe54e5292e94a9f2a518723', '乌鲁木齐', 'wulumuqi', '0032812', '12', '2fabed91c6d94e698ed449165cd250ca', '乌鲁木齐', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('33fe30734ed84994bdd46ebe69aac088', '朝阳区', 'chaoyangqu', '0030103', '3', '12a62a3e5bed44bba0412b7e6b733c93', '朝阳区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('34bc05269e304e2e99c9ded314a12321', '石景山区', 'shijingshanqu', '0030105', '5', '12a62a3e5bed44bba0412b7e6b733c93', '石景山区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('34d6634b8baa47a3b1c0d0346d93873b', '和平区', 'hepingqu', '0032601', '1', '2c254799d3454f2cbc338ef5712548e9', '和平区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3521e41344aa42aaa1cd212482992055', '漯河', 'luohe', '0031207', '7', '7336944efb4b40fcae9118fc9a970d2d', '漯河市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('38bc876c1cab4434af9d14be194463c8', '南充', 'nanchong', '0032514', '14', 'd3538add7125404aba4b0007ef9fde50', '南充市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('39595ea4b765445dae9c26ae870b3a0f', '克州', 'kezhou', '0032809', '9', '2fabed91c6d94e698ed449165cd250ca', '克州', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3a3b4ea7445a4aec80083e5957028990', '汕头', 'shantou', '0030712', '12', '0dd1f40bcb9d46aeba015dc19645a5b9', '汕头市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3a5d1b6e800541c683724672cae3e0f6', '泰安', 'taian', '0030312', '12', '10f46a521ea0471f8d71ee75ac3b5f3a', '泰安市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3ad7c52e9a7044a1a9ab00f29f8cef7c', '阿克苏', 'a\'kesu', '0032801', '1', '2fabed91c6d94e698ed449165cd250ca', '阿克苏', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3ae7c64c40c147eeb3898883e20a7fe0', '山南', 'shannan', '0032707', '7', '3e846b08dbbe495e93bc93f8f202de79', '山南', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3d2a8f11e6d345b5af2f8e5d8bb6bb7a', '鹰潭', 'yingtan', '0031811', '11', 'cb3008cbd6ae4b5f8cebd2254ccb8603', '鹰潭市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3dbed4444dfc4884ab57d769ceac9507', '松江区', 'songjiangqu', '0030213', '13', 'f1ea30ddef1340609c35c88fb2919bee', '松江区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3e846b08dbbe495e93bc93f8f202de79', '西藏', 'xizang', '00327', '27', '1', '西藏', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3eed1c5fb2c9420dbe6e76fdb0f9c4cb', '眉山', 'meishan', '0032511', '11', 'd3538add7125404aba4b0007ef9fde50', '眉山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3f7c5e8ba51849549f5e5dfee72566cc', '呼伦贝尔', 'hulunbeier', '0032007', '7', 'c072c248c7ab47dda7bf24f5e577925c', '呼伦贝尔市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3fb6c72b11124211a22d9f8f40715737', '辽阳', 'liaoyang', '0031910', '10', 'b3366626c66c4b61881f09e1722e8495', '辽阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('3fffacbb502d4647bd358ff00412f536', '长治', 'changzhi', '0032301', '1', '023473e9e6204583a110531036357514', '长治市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('407fa7f152f4461582cfd6904b2c454a', '晋城', 'jincheng', '0032303', '3', '023473e9e6204583a110531036357514', '晋城市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('415fe5fbf3054a3ea2ebdbe24ce4c49f', '永川区', 'yongchuanqu', '0033118', '18', '1c85fbd06cf840d093f3640aca1b6b2d', '永川区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('417d25314a9e43c6b7b725db160db360', '延安', 'yan\'an', '0032409', '9', '534850c72ceb4a57b7dc269da63c330a', '延安市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('4266f08d4bc24321bba7ea3a83a8ba95', '永州', 'yongzhou', '0031511', '11', 'c59f91630bef4289b71fcb2a48994582', '永州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('429dc62e0f6641b8b2ddced272d5d087', '遵义', 'zunyi', '0030909', '9', '592f6fcf45a74524aa8ea853fc9761d5', '遵义市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('432c0a1be10143beba6de15ad8811b64', '安顺', 'anshun', '0030901', '1', '592f6fcf45a74524aa8ea853fc9761d5', '安顺市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('44fee1b9a9e141f9bdf90053f4972d2e', '北辰区', 'beichenqu', '0032610', '10', '2c254799d3454f2cbc338ef5712548e9', '北辰区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('458411aef2d34ccd99ab5976f0f1f030', '金华', 'jinhua', '0033004', '4', '6d846178376549ed878d11d109819f25', '金华市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('45a5be4b22ec494c99b112a7c96bca47', '钦州', 'qinzhou', '0030812', '12', 'c5f3d426c582410281f89f1451e1d854', '钦州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('4795a00ae89441ce82bcabdf086e8316', '宝鸡', 'baoji', '0032402', '2', '534850c72ceb4a57b7dc269da63c330a', '宝鸡市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('480875fd43a947119e24c2302eeead53', '伊春', 'yichun', '0031313', '13', 'b2d4133b5dbf4599ada940620d2ab250', '伊春市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('4921e0e6f9d445cdb6a4b3da98ab3555', '林芝', 'linzhi', '0032704', '4', '3e846b08dbbe495e93bc93f8f202de79', '林芝', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('4972af008f074efd91ea8312587afb42', '牡丹江', 'mudanjiang', '0031308', '8', 'b2d4133b5dbf4599ada940620d2ab250', '牡丹江市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('49b4639e83e441c581bfdefda3a9ac27', '宜春', 'yichun', '0031810', '10', 'cb3008cbd6ae4b5f8cebd2254ccb8603', '宜春市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('4b41fbe4cdae414a91af371e3105ebe5', '景德镇', 'jingdezhen', '0031804', '4', 'cb3008cbd6ae4b5f8cebd2254ccb8603', '景德镇市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('4b72f022312e4664ae7863b343239ff0', '淄博', 'zibo', '0030317', '17', '10f46a521ea0471f8d71ee75ac3b5f3a', '淄博市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('4be3e0a560a2486eae928b44110e971e', '白城', 'baicheng', '0031601', '1', '857be71b0d6d4a40a2c83476824206d1', '白城市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('4c5dbcb293bf40f8837c0acec1ad67eb', '佳木斯', 'jiamusi', '0031307', '7', 'b2d4133b5dbf4599ada940620d2ab250', '佳木斯市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('4cee922697a64ec78de69210e8e40af1', '昌都', 'changdu', '0032702', '2', '3e846b08dbbe495e93bc93f8f202de79', '昌都', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('50b42656af3f4068984fa042e81d7d22', '威海', 'weihai', '0030313', '13', '10f46a521ea0471f8d71ee75ac3b5f3a', '威海市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('510607a1836e4079b3103e14ec5864ed', '云南', 'yunnan', '00329', '29', '1', '云南省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('512a316326984ed8aa32d7f610b7604f', '玉树', 'yushu', '0032208', '8', '5a80e3435c0e4dc09bafceeadb38e5f0', '玉树', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('51f23688604848809184ec828f89cfca', '嘉兴', 'jiaxing', '0033003', '3', '6d846178376549ed878d11d109819f25', '嘉兴市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('5259c4093aa84f7c88a367685581fbc6', '门头沟区', 'mentougouqu', '0030107', '7', '12a62a3e5bed44bba0412b7e6b733c93', '门头沟区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('52630830669149edba48a7bb9b06e297', '中卫', 'zhongwei', '0032106', '6', '5690b0534fe745e5ba0f504f0c260559', '中卫市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('534850c72ceb4a57b7dc269da63c330a', '陕西', 'shanxi', '00324', '24', '1', '陕西省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('537974fdf5f54b8f99452bb8a03cf37b', '厦门', 'xiamen', '0030508', '8', 'd4066f6f425a4894a77f49f539f2a34f', '厦门市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('53ca3f5aede8420b835b38bbc542da81', '松源', 'songyuan', '0031607', '7', '857be71b0d6d4a40a2c83476824206d1', '松源市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('558d920174014b4cb5a0c8f518b5819b', '东城区', 'dongchengqu', '0030101', '1', '12a62a3e5bed44bba0412b7e6b733c93', '东城区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('56103b0e83a44d07a6025061fab4cebc', '运城', 'yuncheng', '0032311', '11', '023473e9e6204583a110531036357514', '运城市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('5690b0534fe745e5ba0f504f0c260559', '宁夏', 'ningxia', '00321', '21', '1', '宁夏', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('576fa3bd7d294f61af1315e95f70d44c', '九龙坡区', 'jiulongpoqu', '0033107', '7', '1c85fbd06cf840d093f3640aca1b6b2d', '九龙坡区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('577405ff648240959b3765c950598ab0', '江苏', 'jiangsu', '00317', '17', '1', '江苏省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('58ed29aefce044339ecf067f514c43cb', '宁德', 'ningde', '0030504', '4', 'd4066f6f425a4894a77f49f539f2a34f', '宁德市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('5908fbf750a347b8a6b82af7778bb866', '商洛', 'shangluo', '0032404', '4', '534850c72ceb4a57b7dc269da63c330a', '商洛市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('592f6fcf45a74524aa8ea853fc9761d5', '贵州', 'guizhou', '00309', '9', '1', '贵州省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('59dbb93b007d44a6ae97744ac14e642c', '龙岩', 'longyan', '0030502', '2', 'd4066f6f425a4894a77f49f539f2a34f', '龙岩市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('5a80e3435c0e4dc09bafceeadb38e5f0', '青海', 'qinghai', '00322', '22', '1', '青海', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('5b5747166f714882b5cdfbeb7856f965', '周口', 'zhoukou', '0031217', '17', '7336944efb4b40fcae9118fc9a970d2d', '周口市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('5c91965168eb4deaab99266bbb4b64e1', '朝阳', 'chaoyang', '0031903', '3', 'b3366626c66c4b61881f09e1722e8495', '朝阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('5d2d367b0aee49449e65d0dd7601ee29', '巴中', 'bazhong', '0032502', '2', 'd3538add7125404aba4b0007ef9fde50', '巴中市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('5ddde49610d4433eb157897d01ba6838', '东莞', 'dongguan', '0030702', '2', '0dd1f40bcb9d46aeba015dc19645a5b9', '东莞市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('5e85d09db1054472bcca22d82da6ec5d', '乐山', 'leshan', '0032509', '9', 'd3538add7125404aba4b0007ef9fde50', '乐山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('60c96ec0debf4cc4bf5974c93d4f638c', '阳泉', 'yangquan', '0032310', '10', '023473e9e6204583a110531036357514', '阳泉市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('61c3711334fe44b0806e717e6fb238b0', '滨海新区', 'binhaixinqu', '0032613', '13', '2c254799d3454f2cbc338ef5712548e9', '滨海新区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6214f40ad2704fb6af0ded59420ca958', '安康', 'ankang', '0032401', '1', '534850c72ceb4a57b7dc269da63c330a', '安康市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('62ed6854726c4674ae2f5b676ddec7fb', '新乡', 'xinxiang', '0031213', '13', '7336944efb4b40fcae9118fc9a970d2d', '新乡市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6303e3eeffb441018044b039063e3f96', '丹东', 'dandong', '0031905', '5', 'b3366626c66c4b61881f09e1722e8495', '丹东市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('644316de71f942f9a90eb1f810eca872', '怀柔区', 'huairouqu', '0030113', '13', '12a62a3e5bed44bba0412b7e6b733c93', '怀柔区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('64a03236dd084d09ada9a1ca22b3815f', '长春', 'changchun', '0031603', '3', '857be71b0d6d4a40a2c83476824206d1', '长春市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('65629a05de764a19b66b752726f5cdbd', '海淀区', 'haidianqu', '0030106', '6', '12a62a3e5bed44bba0412b7e6b733c93', '海淀区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6567a081f5d142779e17edbda3da9a04', '宁波', 'ningbo', '0033006', '6', '6d846178376549ed878d11d109819f25', '宁波市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6624ad3b318149f3a8ee5beef1b8b38f', '肇庆', 'zhaoqing', '0030719', '19', '0dd1f40bcb9d46aeba015dc19645a5b9', '肇庆市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('674ec37e9641450dadc9798df10c58bc', '静安区', 'jinganqu', '0030204', '4', 'f1ea30ddef1340609c35c88fb2919bee', '静安区', '', '', null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('67cba9a4ca4c4c38ac3ba2c21dd191e6', '南昌', 'nanchang', '0031806', '6', 'cb3008cbd6ae4b5f8cebd2254ccb8603', '南昌市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('68a8f7a8337141d3a092fadfd2457970', '鸡西', 'jixi', '0031306', '6', 'b2d4133b5dbf4599ada940620d2ab250', '鸡西市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6932b6b7b5124bef8385fb8e5b5c2568', '通化', 'tonghua', '0031608', '8', '857be71b0d6d4a40a2c83476824206d1', '通化市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('694348f8e1c0444e8e0b2c2caf4de1a6', '榆林', 'yulin', '0032410', '10', '534850c72ceb4a57b7dc269da63c330a', '榆林市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('697141b58ada46518bc8ec0cc3d64b31', '博州', 'bozhou', '032804', '4', '2fabed91c6d94e698ed449165cd250ca', '博州', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6a2226c73bc745faa6973dd3af3e274a', '吐鲁番', 'tulufan', '0032811', '11', '2fabed91c6d94e698ed449165cd250ca', '吐鲁番', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6b2b1d55b06b44cd8a487d327397b69b', '徐汇区', 'xuhuiqu', '0030202', '2', 'f1ea30ddef1340609c35c88fb2919bee', '徐汇区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6d1e9b9c9c334448878312d589eeaeac', '荣昌区', 'rongchangqu', '0033123', '23', '1c85fbd06cf840d093f3640aca1b6b2d', '荣昌区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6d846178376549ed878d11d109819f25', '浙江', 'zhejiang', '00330', '30', '1', '浙江省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6daab50a4a1048f993f348a66dcfa83d', '喀什', 'kashi', '0032807', '7', '2fabed91c6d94e698ed449165cd250ca', '喀什', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6dc38f66c86a4f32ac9d585e668c110e', '怒江', 'nujiang', '0032911', '11', '510607a1836e4079b3103e14ec5864ed', '怒江', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6e1f1c6d82704e5cadcd0bc8ef2ab79f', '昆明', 'kunming', '0032908', '8', '510607a1836e4079b3103e14ec5864ed', '昆明', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6e639147d90943c38490cafe223985ce', '梅州', 'meizhou', '0030710', '10', '0dd1f40bcb9d46aeba015dc19645a5b9', '梅州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6e67518f1da44dbaa8cf95920779f188', '汕尾', 'shanwei', '0030713', '13', '0dd1f40bcb9d46aeba015dc19645a5b9', '汕尾市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6eaa823330da42b6b5783e389707853c', '四平', 'siping', '0031606', '6', '857be71b0d6d4a40a2c83476824206d1', '四平市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6ecc40e527404bba89207cc158ef3994', '河源', 'heyuan', '0030705', '5', '0dd1f40bcb9d46aeba015dc19645a5b9', '河源市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6ee20f02066e43a29f10ca6dbd0b7c71', '普陀区', 'putuoqu', '0030205', '5', 'f1ea30ddef1340609c35c88fb2919bee', '普陀区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6f5749ab2b5d4cbea1655e9a5197096d', '湛江', 'zhanjiang', '0030718', '18', '0dd1f40bcb9d46aeba015dc19645a5b9', '湛江市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6f8c18c8b3a54bc287c1dfc5642be577', '三明', 'sanming', '0030507', '7', 'd4066f6f425a4894a77f49f539f2a34f', '三明市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6f9601270aca46519e7f8836e0d2446c', '广元', 'guangyuan', '0032508', '8', 'd3538add7125404aba4b0007ef9fde50', '广元市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('6fd083505ab24086b234c032dab3c2a7', '海口', 'haikou', '0031001', '1', '2ba8e6d0fd944983aa19b781c6b53477', '海口市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('70733399b60d4b058c255fa9fff2eee0', '莆田', 'putian', '0030505', '5', 'd4066f6f425a4894a77f49f539f2a34f', '莆田市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('70c1525174a04767865d0e6b7ed01f5a', '七台河', 'qitaihe', '0031309', '9', 'b2d4133b5dbf4599ada940620d2ab250', '七台河市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('70c41ec5cb9e4aec98bd2357702c4082', '江门', 'jiangmen', '0030707', '7', '0dd1f40bcb9d46aeba015dc19645a5b9', '江门市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('71aba068cd5b4588a03be75e2e49f496', '鄂州', 'ezhou', '0031401', '1', '312b80775e104ba08c8244a042a658df', '鄂州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('7336944efb4b40fcae9118fc9a970d2d', '河南', 'henan', '00312', '12', '1', '河南省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('748623f3282b4ca7ace0e73303327310', '巴南区', 'bananqu', '0033113', '13', '1c85fbd06cf840d093f3640aca1b6b2d', '巴南区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('74d2aaddaf294355b01970d52e303a1a', '资阳', 'ziyang', '0032519', '19', 'd3538add7125404aba4b0007ef9fde50', '资阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('75362368f22f4d60a810c2a45cced487', '河北', 'hebei', '00311', '11', '1', '河北省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('755e2d15540c49dbad6be564f694a4af', '曲靖', 'qujing', '0032913', '13', '510607a1836e4079b3103e14ec5864ed', '曲靖', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('75b889c8e86c4d518a1fb74b089ceae8', '攀枝花', 'panzhihua', '0032515', '15', 'd3538add7125404aba4b0007ef9fde50', '攀枝花市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('75e0334ad60b41a1b42ae6724b06c874', '镇江', 'zhenjiang', '0031713', '13', '577405ff648240959b3765c950598ab0', '镇江市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('762bc3b1443e4ea98ab051b4007c0238', '邢台', 'xingtai', '0031110', '10', '75362368f22f4d60a810c2a45cced487', '邢台市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('76d023f160e448c8bcb78598bf246a44', '黔南', 'qiannan', '0030906', '6', '592f6fcf45a74524aa8ea853fc9761d5', '黔南市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('773cb4f25b9d4ebcba2953570da776c9', '吴忠', 'wuzhong', '0032104', '4', '5690b0534fe745e5ba0f504f0c260559', '吴忠市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('776b55acb6804296a00c9f97723633ba', '舟山', 'zhoushan', '0033011', '11', '6d846178376549ed878d11d109819f25', '舟山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('79b98f7f0c054fa0ab2a3a2cb75d1b87', '邵阳', 'shaoyang', '0031505', '5', 'c59f91630bef4289b71fcb2a48994582', '邵阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('7a8097646dc8419284201db66dd6eda1', '芜湖', 'wuhu', '0030416', '16', '249999f296d14f95b8138a30bbb2c374', '芜湖市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('7ab1618b70354ee2ab49e8fd5cbca27f', '哈密', 'hami', '0032806', '6', '2fabed91c6d94e698ed449165cd250ca', '哈密', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('7bed9f7f137e4048bbfd0d564283312d', '日照', 'rizhao', '0030311', '11', '10f46a521ea0471f8d71ee75ac3b5f3a', '日照市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('7c6addc8becd4e759479228f6dd38bb2', '通辽', 'tongliao', '0032008', '8', 'c072c248c7ab47dda7bf24f5e577925c', '通辽市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('7f69cfd9e1ae4c92b4ddf13b9f78cb6c', '保山', 'baoshan', '0032902', '2', '510607a1836e4079b3103e14ec5864ed', '保山', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('802ef5c62bbb47e3a026e3c92989f53e', '临夏', 'linxia', '0030608', '8', '3283f1a77180495f9a0b192d0f9cdd35', '临夏市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8114568fa8a34c6e878ff13d5ba59006', '齐齐哈尔', 'qiqihaer', '0031310', '10', 'b2d4133b5dbf4599ada940620d2ab250', '齐齐哈尔市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8127fbeb13a44c3284dfa8e2326ce19a', '茂名', 'maoming', '0030709', '9', '0dd1f40bcb9d46aeba015dc19645a5b9', '茂名市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('813ff1376c0445c6a64b9f00452c2427', '成都', 'chengdu', '0032503', '3', 'd3538add7125404aba4b0007ef9fde50', '成都市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('81443e6b687349c6ae3231aff5e038ba', '铜陵', 'tongling', '0030415', '15', '249999f296d14f95b8138a30bbb2c374', '铜陵市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8170c3271bc840d78e92ed355851aa5e', '西城区', 'xichengqu', '0030102', '2', '12a62a3e5bed44bba0412b7e6b733c93', '西城区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8346ebddc2464a9bbb99f7b0794da39c', '韶关', 'shaoguan', '0030714', '14', '0dd1f40bcb9d46aeba015dc19645a5b9', '韶关市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8362c89358c748a5907b44de500a1333', '泰州', 'taizhou', '0031708', '8', '577405ff648240959b3765c950598ab0', '泰州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('83e9fbd9e7fa4d878575088df7798b5e', '泸州', 'luzhou', '0032510', '10', 'd3538add7125404aba4b0007ef9fde50', '泸州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('857be71b0d6d4a40a2c83476824206d1', '吉林', 'jilin', '00316', '16', '1', '吉林省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('85b8accc31b34d70bce548a9d42767dd', '汉中', 'hanzhong', '0032403', '3', '534850c72ceb4a57b7dc269da63c330a', '汉中市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('85c0cb3849bc4d79b9c2fa2b63b2c858', '福州', 'fuzhou', '0030501', '1', 'd4066f6f425a4894a77f49f539f2a34f', '福州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('85da31513d984f3e8a179b764efb3a22', '太原', 'taiyuan', '0032308', '8', '023473e9e6204583a110531036357514', '太原市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('863205a0ac1d4c50b19bb79f602dbea7', '张家口', 'zhangjiakou', '0031111', '11', '75362368f22f4d60a810c2a45cced487', '张家口市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8636d6e9bdb34510bcb528159ca4f29d', '辽源', 'liaoyuan', '0031605', '5', '857be71b0d6d4a40a2c83476824206d1', '辽源市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('866bf0b4a8cc41dfb5071f8edb271934', '佛山', 'foshan', '0030703', '3', '0dd1f40bcb9d46aeba015dc19645a5b9', '佛山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('86b85d3d8ddc4632946bdc4cdf642504', '虹口区', 'hongkouqu', '0030206', '6', 'f1ea30ddef1340609c35c88fb2919bee', '虹口区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('871c4e3b4a044b2e8235d731835db559', '酒泉', 'jiuquan', '0030606', '6', '3283f1a77180495f9a0b192d0f9cdd35', '酒泉市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('87563959aa914187a0b5af82f862a0f0', '金昌', 'jinchang', '0030605', '5', '3283f1a77180495f9a0b192d0f9cdd35', '金昌市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('886a63c7def64cdfad1cfc0a2de8a1e0', '崇左', 'chongzuo', '0030803', '3', 'c5f3d426c582410281f89f1451e1d854', '崇左市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('89341f9a48444d258609e87cf40604fa', '伊犁州直', 'yilizhouzhi', '0032813', '13', '2fabed91c6d94e698ed449165cd250ca', '伊犁州直', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('89461222215e40f7b8067c7b791a9c2c', '南平', 'nanping', '0030503', '3', 'd4066f6f425a4894a77f49f539f2a34f', '南平市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('895262136b904f4888aa5af2f89dc967', '洛阳', 'luoyang', '0031206', '6', '7336944efb4b40fcae9118fc9a970d2d', '洛阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('896bce499bd740ffb9f745a4782a7886', '青浦区', 'qingpuqu', '0030214', '14', 'f1ea30ddef1340609c35c88fb2919bee', '青浦区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8a7265e11f0141ba808c0410b76d415b', '临沂', 'linyi', '0030309', '9', '10f46a521ea0471f8d71ee75ac3b5f3a', '临沂市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8a79e7d2af7244b0b543492374ec6549', '兴安盟', 'xinganmeng', '0032012', '12', 'c072c248c7ab47dda7bf24f5e577925c', '兴安盟', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8abbfbb071f34df4b77e2828f53ef99b', '怀化', 'huaihua', '0031512', '12', 'c59f91630bef4289b71fcb2a48994582', '怀化市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8af40c23c6fe4ed8819dbe99f1f125f0', '奉贤区', 'fengxianqu', '0030215', '15', 'f1ea30ddef1340609c35c88fb2919bee', '奉贤区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8b336fb55c2346a2b5ec13f578c627ef', '苏州', 'suzhou', '0031706', '6', '577405ff648240959b3765c950598ab0', '苏州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8b5512281c364e09a67d8e81a5bd7ce9', '南川区', 'nanchuanqu', '0033119', '19', '1c85fbd06cf840d093f3640aca1b6b2d', '南川区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8b95528d20c44f86adf1e64009ce317b', '贵阳', 'guiyang', '0030903', '3', '592f6fcf45a74524aa8ea853fc9761d5', '贵阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8bfd173e34704a7f978c539f87a511a8', '宝山区', 'baoshanqu', '0030209', '9', 'f1ea30ddef1340609c35c88fb2919bee', '宝山区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8c64bc307e894223a429a4f50a0cd387', '唐山', 'tangshan', '0031109', '9', '75362368f22f4d60a810c2a45cced487', '唐山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8d10532fba444c66bead45a6d9e13b6a', '延庆区', 'yanqingqu', '0030116', '16', '12a62a3e5bed44bba0412b7e6b733c93', '延庆区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8e35c978f8b248cb93863818be6be56b', '大足区', 'dazuqu', '0033111', '11', '1c85fbd06cf840d093f3640aca1b6b2d', '大足区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8f617ff8e61c49689cb26540a618a80c', '宁河区', 'ninghequ', '0032614', '14', '2c254799d3454f2cbc338ef5712548e9', '宁河区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('8fa97a231db54e879ece49d566f0561d', '天门', 'tianmen', '0031410', '10', '312b80775e104ba08c8244a042a658df', '天门市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('9068c4ec7d1a4de69339fb61654cb3d9', '温州', 'wenzhou', '0033010', '10', '6d846178376549ed878d11d109819f25', '温州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('921a9e3d2c434cca943aca4f3e5087b5', '渝中区', 'yuzhongqu', '0033103', '3', '1c85fbd06cf840d093f3640aca1b6b2d', '渝中区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('923ca61a8fdb4357a5220763fdbd7c37', '台州', 'taizhou', '0033009', '9', '6d846178376549ed878d11d109819f25', '台州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('92800c5b33df4f15a689ceda6bd23f2b', '信阳', 'xinyang', '0031214', '14', '7336944efb4b40fcae9118fc9a970d2d', '信阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('9381ab9da4b64001b289252ee21d1157', '河东区', 'hedongqu', '0032602', '2', '2c254799d3454f2cbc338ef5712548e9', '河东区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('95a7fd77484f4ef39e9ed4596556a93c', '海西', 'haixi', '0032205', '5', '5a80e3435c0e4dc09bafceeadb38e5f0', '海西', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('9795de38301642539aefda61adf595a4', '江津区', 'jiangjinqu', '0033116', '16', '1c85fbd06cf840d093f3640aca1b6b2d', '江津区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('97c543ee46e84586998554f5f745fcc6', '杨浦区', 'yangpuqu', '0030207', '7', 'f1ea30ddef1340609c35c88fb2919bee', '杨浦区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('983eaae633244ecea99d11a804b1c736', '万州区', 'wanzhouqu', '0033101', '1', '1c85fbd06cf840d093f3640aca1b6b2d', '万州区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('9a7465597dda46c8bb4c5c98aabfb4b9', '阳江', 'yangjiang', '0030716', '16', '0dd1f40bcb9d46aeba015dc19645a5b9', '阳江市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('9b483ad27bc14af2a47d8facdf8fafca', '嘉峪关', 'jiayuguan', '0030604', '4', '3283f1a77180495f9a0b192d0f9cdd35', '嘉峪关市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('9bb5220b50dd4def87ffbf6444a28c58', '黄浦区', 'huangpuqu', '0030201', '1', 'f1ea30ddef1340609c35c88fb2919bee', '黄浦区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('9c8a7d5f3423458eb9e6ef68f6185fca', '黄山', 'huangshan', '0030411', '11', '249999f296d14f95b8138a30bbb2c374', '黄山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('9d9700b28ae347bca4db9f592c78eb02', '百色', 'baise', '0030801', '1', 'c5f3d426c582410281f89f1451e1d854', '百色市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('9e4d7c5d9d86458d8c8d8a644e7eec9a', '阿拉善盟', 'alashanmeng', '0032001', '1', 'c072c248c7ab47dda7bf24f5e577925c', '阿拉善盟', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('9e833df814a74d1690f8039782ddf914', '昭通', 'zhaotong', '0032916', '16', '510607a1836e4079b3103e14ec5864ed', '昭通', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('9e9d0ad23c9e45df9dd9c269c0e4fdfa', '昌平区', 'changpingqu', '0030111', '11', '12a62a3e5bed44bba0412b7e6b733c93', '昌平区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('9fc24347a7ca4a34bdea408dad223348', '闵行区', 'minhangqu', '0030208', '8', 'f1ea30ddef1340609c35c88fb2919bee', '闵行区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a013e3db1b384beb843959c33f361203', '静海区', 'jinghaiqu', '0032615', '15', '2c254799d3454f2cbc338ef5712548e9', '静海区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a0d5b55e48c945faad1d7bb624de7de8', '九江', 'jiujiang', '0031805', '5', 'cb3008cbd6ae4b5f8cebd2254ccb8603', '九江市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a165da9b81b940fe9764fc7f5d41232d', '银川', 'yinchuan', '0032105', '5', '5690b0534fe745e5ba0f504f0c260559', '银川市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a268c4b698274f12a64ba48db568d057', '潮州', 'chaozhou', '0030701', '1', '0dd1f40bcb9d46aeba015dc19645a5b9', '潮州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a34815cb348d4598a6fac4ece3baa0cd', '衡阳', 'hengyang', '0031504', '4', 'c59f91630bef4289b71fcb2a48994582', '衡阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a39af97e7ad04ebfb530f49b05c7b146', '阜阳', 'fuyang', '0030407', '7', '249999f296d14f95b8138a30bbb2c374', '阜阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a405680276e645188a122e8933f77a38', '乌兰察布', 'wulanchabu', '0032010', '10', 'c072c248c7ab47dda7bf24f5e577925c', '乌兰察布市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a46bb0749dac4627b9a7d465dc75aca5', '武汉', 'wuhan', '0031411', '11', '312b80775e104ba08c8244a042a658df', '武汉市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a4d4d8f678dd4dba958f466bbc581c5f', '菏泽', 'heze', '0030305', '5', '10f46a521ea0471f8d71ee75ac3b5f3a', '菏泽市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a6898561d8664f6dad7a32f63ab64e19', '铁岭', 'tieling', '0031913', '13', 'b3366626c66c4b61881f09e1722e8495', '铁岭市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a76ed518fb584442aaf1769fd2583677', '兰州', 'lanzhou', '0030607', '7', '3283f1a77180495f9a0b192d0f9cdd35', '兰州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a7d500ab05844c45b839f4a30c1d7643', '宜昌', 'yichang', '0031416', '16', '312b80775e104ba08c8244a042a658df', '宜昌市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('a9093795a013403e869a4308f17c7588', '海东', 'haidong', '0032203', '3', '5a80e3435c0e4dc09bafceeadb38e5f0', '海东', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('aa4b703a13dc4116bea578295efd9ea0', '赤峰', 'chifeng', '0032004', '4', 'c072c248c7ab47dda7bf24f5e577925c', '赤峰市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('aab8d56a03de4bbc84d433bc24748730', '贺州', 'hezhou', '0030808', '8', 'c5f3d426c582410281f89f1451e1d854', '贺州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ac1aa3c1b928467ebfa261cfaeb77be9', '葫芦岛', 'huludao', '0031908', '8', 'b3366626c66c4b61881f09e1722e8495', '葫芦岛市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ac23d537ccd64827ab44007c5503bd58', '安阳', 'anyang', '0031201', '1', '7336944efb4b40fcae9118fc9a970d2d', '安阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('acd9f9b8fb8a4e47bd1e5d4eea45809c', '滁州', 'chuzhou', '0030406', '6', '249999f296d14f95b8138a30bbb2c374', '滁州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('acf5c250d6614fb9920e442a3b178b86', '东营', 'dongying', '0030304', '4', '10f46a521ea0471f8d71ee75ac3b5f3a', '东营市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('adfac2a66ce04767bdbabbd8c115cd5d', '黄南', 'huangnan', '0032206', '6', '5a80e3435c0e4dc09bafceeadb38e5f0', '黄南', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ae2c4a00360442f29ce8b0c284525ded', '崇明县', 'chongmingxian', '0030216', '16', 'f1ea30ddef1340609c35c88fb2919bee', '崇明县', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ae384d9313e448949c8ed9c565e5cade', '扬州', 'yangzhou', '0031712', '12', '577405ff648240959b3765c950598ab0', '扬州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('affc6a796b524efdb006bd1730003df7', '西青区', 'xiqingqu', '0032608', '8', '2c254799d3454f2cbc338ef5712548e9', '西青区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b0251d85679b40dca30ee83af80838bb', '拉萨', 'lasa', '0032703', '3', '3e846b08dbbe495e93bc93f8f202de79', '拉萨市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b041a523ae214f03969454aa8c180ced', '房山区', 'fangshanqu', '0030108', '8', '12a62a3e5bed44bba0412b7e6b733c93', '房山区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b08ccea3cf89458e97b346546221e4ae', '凉山', 'liangshan', '0032102', '2', '5690b0534fe745e5ba0f504f0c260559', '凉山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b1d50a7eb21f44389733e17831fd121d', '盘锦', 'panjin', '0031911', '11', 'b3366626c66c4b61881f09e1722e8495', '盘锦市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b21a81793ca6459f97c246ccbd543c67', '张掖', 'zhangye', '0030614', '14', '3283f1a77180495f9a0b192d0f9cdd35', '张掖市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b2356bf7a1d546709ac296de1bf2a9eb', '连云港', 'lianyungang', '0031703', '3', '577405ff648240959b3765c950598ab0', '连云港市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b2409f9c928f4dd1bd224809f54a1225', '宝坻区', 'baodiqu', '0032612', '12', '2c254799d3454f2cbc338ef5712548e9', '宝坻区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b2d4133b5dbf4599ada940620d2ab250', '黑龙江', 'heilongjiang', '00313', '13', '1', '黑龙江省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b3221428d30249d8acbb40f0f38d7a5c', '南宁', 'nanning', '0030811', '11', 'c5f3d426c582410281f89f1451e1d854', '南宁市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b3366626c66c4b61881f09e1722e8495', '辽宁', 'liaoning', '00319', '19', '1', '辽宁省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b38f0725533a47cea5d0f5f520ad72c7', '沙坪坝区', 'shapingbaqu', '0033106', '6', '1c85fbd06cf840d093f3640aca1b6b2d', '沙坪坝区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b4736703fc064dbe8b8440c79991a1ed', '大连', 'dalian', '0031904', '4', 'b3366626c66c4b61881f09e1722e8495', '大连市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b5329030086d470fa8cf6b38aaafb320', '随州', 'suizhou', '0031409', '9', '312b80775e104ba08c8244a042a658df', '随州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b68e669c229945ae86e053d15c277a6a', '惠州', 'huizhou', '0030706', '6', '0dd1f40bcb9d46aeba015dc19645a5b9', '惠州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b7785f96730e4a35820e08da1c200c4d', '璧山区', 'bishanqu', '0033120', '20', '1c85fbd06cf840d093f3640aca1b6b2d', '璧山区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b84acd830f3b4c65bd82c97cc925badf', '自贡', 'zigong', '0032520', '20', 'd3538add7125404aba4b0007ef9fde50', '自贡市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b8e203af46924284a9a8be9851a557a2', '南开区', 'nankaiqu', '0032604', '4', '2c254799d3454f2cbc338ef5712548e9', '南开区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b908e46ac1544cb6a26f1e1fb22f2a94', '宜宾', 'yibin', '0032518', '18', 'd3538add7125404aba4b0007ef9fde50', '宜宾市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('b9c02b885a4a49719b1000110ed47df4', '毕节', 'bijie', '0030902', '2', '592f6fcf45a74524aa8ea853fc9761d5', '毕节市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ba821dcfd50d4f64af1cd3eecc5f54e0', '海北', 'haibei', '0032202', '2', '5a80e3435c0e4dc09bafceeadb38e5f0', '海北', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('bbf9ff3b0fa444f18d70f2a4a9e45609', '绵阳', 'mianyang', '0032512', '12', 'd3538add7125404aba4b0007ef9fde50', '绵阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('bc2ec49f78204ea29cd666e2dc6583a2', '临沧', 'lincang', '0032910', '10', '510607a1836e4079b3103e14ec5864ed', '临沧', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('bc97087ea25547a794cec553d03c1abc', '那曲', 'naqu', '0032705', '5', '3e846b08dbbe495e93bc93f8f202de79', '那曲', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('bd1efd2194724213b72efa91fe3d5ddc', '浦东新区', 'pudongxinqu', '0030211', '11', 'f1ea30ddef1340609c35c88fb2919bee', '浦东新区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('bdb65b22a7c447dcadbc6328292e5aef', '中山', 'zhongshan', '0030720', '20', '0dd1f40bcb9d46aeba015dc19645a5b9', '中山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('bdce443b39ba4cef8c0c0b75bdc8e253', '沈阳', 'shenyang', '0031912', '12', 'b3366626c66c4b61881f09e1722e8495', '沈阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('be33f1cad2954520bbf033e4198890f7', '亳州', 'bozhou', '0030403', '3', '249999f296d14f95b8138a30bbb2c374', '亳州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('be359cbd02944e1da5997ae560831db1', '定西', 'dingxi', '0030602', '2', '3283f1a77180495f9a0b192d0f9cdd35', '定西市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('beaf0090ebf94ad9af5dd78e372611fe', '咸阳', 'xianyang', '0032408', '8', '534850c72ceb4a57b7dc269da63c330a', '咸阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('bff8ba692e4c4d78a23309ec0ad745c7', '桂林', 'guilin', '0030806', '6', 'c5f3d426c582410281f89f1451e1d854', '桂林市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c0170db89f2e48fe99bfdcd857fe2016', '承德', 'chengde', '0031103', '3', '75362368f22f4d60a810c2a45cced487', '承德市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c072c248c7ab47dda7bf24f5e577925c', '内蒙古', 'neimenggu', '00320', '20', '1', '内蒙古', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c0e55c6a60564359859d87d25c249ac4', '锦州', 'jinzhou', '0031909', '9', 'b3366626c66c4b61881f09e1722e8495', '锦州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c1875ba50f6d4e61870be000be8ee78e', '东丽区', 'dongliqu', '0032607', '7', '2c254799d3454f2cbc338ef5712548e9', '东丽区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c22083b403ba4ea698ba4dfc7245a317', '平凉', 'pingliang', '0030610', '10', '3283f1a77180495f9a0b192d0f9cdd35', '平凉市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c256624d6caa49979ebc4dce35006945', '武清区', 'wuqingqu', '0032611', '11', '2c254799d3454f2cbc338ef5712548e9', '武清区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c2e0fca8943d41ba8ec5d013e9bc3f41', '日喀则', 'rikaze', '0032706', '6', '3e846b08dbbe495e93bc93f8f202de79', '日喀则', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c3717fb4891148a0bef623dbd746e7e6', '开封', 'kaifeng', '0031205', '5', '7336944efb4b40fcae9118fc9a970d2d', '开封市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c4817fc49be444e491920112aa9a3e32', '淮南', 'huainan', '0030410', '10', '249999f296d14f95b8138a30bbb2c374', '淮南市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c59f91630bef4289b71fcb2a48994582', '湖南', 'hunan', '00315', '15', '1', '湖南省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c5f3d426c582410281f89f1451e1d854', '广西', 'guangxi', '00308', '8', '1', '广西省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c7852784049a473c917863c5bc84dd95', '甘南', 'gannan', '0030603', '3', '3283f1a77180495f9a0b192d0f9cdd35', '甘南市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c7a5ba87961742f3b242ee4d30a55921', '营口', 'yingkou', '0031914', '14', 'b3366626c66c4b61881f09e1722e8495', '营口市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c8342c1dcf584cbf92f20d90a62a34bf', '平顶山', 'pingdingshan', '0031209', '9', '7336944efb4b40fcae9118fc9a970d2d', '平顶山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c8d4119e57f84e71815769f03935e471', '黔东南', 'qiandongnan', '0030905', '5', '592f6fcf45a74524aa8ea853fc9761d5', '黔东南市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c928e9192e2f4f5ca06c6599371ff83c', '六安', 'lu\'an', '0030412', '12', '249999f296d14f95b8138a30bbb2c374', '六安市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c9811aef284b4ae8b8bf7698e90d8b3b', '泉州', 'quanzhou', '0030506', '6', 'd4066f6f425a4894a77f49f539f2a34f', '泉州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('c9df1fd73d0642eea8b050738f6ed9fa', '聊城', 'liaocheng', '0030308', '8', '10f46a521ea0471f8d71ee75ac3b5f3a', '聊城市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ca2e3717bb734c4b9142f29e36a31989', '株洲', 'zhuzhou', '0031502', '2', 'c59f91630bef4289b71fcb2a48994582', '株洲市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('cabe896dba3a4a21ba194f8839a3eb98', '大渡口区', 'dadukouqu', '0033104', '4', '1c85fbd06cf840d093f3640aca1b6b2d', '大渡口区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('caeba09845bf4a29960a840d4f436f09', '上饶', 'shangrao', '0031808', '8', 'cb3008cbd6ae4b5f8cebd2254ccb8603', '上饶市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('cb3008cbd6ae4b5f8cebd2254ccb8603', '江西', 'jiangxi', '00318', '18', '1', '江西省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('cc2aaa3ed3904d66a2f79676b14a1b49', '黔江区', 'qianjiangqu', '0033114', '14', '1c85fbd06cf840d093f3640aca1b6b2d', '黔江区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('cd66a360619847d2b17989643f03dc8f', '济宁', 'jining', '0030306', '6', '10f46a521ea0471f8d71ee75ac3b5f3a', '济宁市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('cd87ffcb742744d18bbce6928922a5be', '衡州', 'hengzhou', '0033007', '7', '6d846178376549ed878d11d109819f25', '衡州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('cdc1df5961994a9d94751003edd0fc58', '宿州', 'suzhou', '0030414', '14', '249999f296d14f95b8138a30bbb2c374', '宿州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('cddd155314404710bd1ab3fa51d80cf2', '天水', 'tianshui', '0030612', '12', '3283f1a77180495f9a0b192d0f9cdd35', '天水市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ce0dcc5a66dd44b3b9a42aef4aa8b4ba', '蚌埠', 'bengbu', '0030402', '2', '249999f296d14f95b8138a30bbb2c374', '蚌埠市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('cf632a4f4de54b619ad5cb6835f35434', '玉林', 'yulin', '0030814', '14', 'c5f3d426c582410281f89f1451e1d854', '玉林市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('cf7d29feb2c34cbfaf4e28896ca577e4', '巴彦淖尔', 'bayannaoer', '0032002', '2', 'c072c248c7ab47dda7bf24f5e577925c', '巴彦淖尔', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('cfb2df83812d4fabb10ba91e98be9467', '黑河', 'heihe', '0031305', '5', 'b2d4133b5dbf4599ada940620d2ab250', '黑河市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('cfee5c9833664e0aba76267389e4adbd', '丰台区', 'fengtaiqu', '0030104', '4', '12a62a3e5bed44bba0412b7e6b733c93', '丰台区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d13b01a607e34fdfa16deb7292a0f299', '鹤壁', 'hebi', '0031202', '2', '7336944efb4b40fcae9118fc9a970d2d', '鹤壁市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d1646344b9cc45018c3a347f6dc6c77b', '红桥区', 'hongqiaoqu', '0032606', '6', '2c254799d3454f2cbc338ef5712548e9', '红桥区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d26fc248d49c4a71b2a003033adc88de', '娄底', 'loudi', '0031513', '13', 'c59f91630bef4289b71fcb2a48994582', '娄底市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d3538add7125404aba4b0007ef9fde50', '四川', 'sichuan', '00325', '25', '1', '四川省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d3c2d3b9184b4e3185ca6bdbe73c5cff', '襄樊', 'xiangfan', '0031414', '14', '312b80775e104ba08c8244a042a658df', '襄樊市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d3c76818757942fba8ebf8246fa257a9', '咸宁', 'xianning', '0031413', '13', '312b80775e104ba08c8244a042a658df', '咸宁市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d4066f6f425a4894a77f49f539f2a34f', '福建', 'fujian', '00305', '5', '1', '福建省', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d4131ecb91d3435db1dbd770ac39221f', '鞍山', 'anshan', '0031901', '1', 'b3366626c66c4b61881f09e1722e8495', '鞍山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d42e09432d10454caecf1d4335aca1da', '合肥', 'hefei', '0030408', '8', '249999f296d14f95b8138a30bbb2c374', '合肥市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d4f8ba23c3ef4fbaa00c8f8a7c047bf1', '商丘', 'shangqiu', '0031212', '12', '7336944efb4b40fcae9118fc9a970d2d', '商丘市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d5f5462779bc4976a3fbcbdeba45ed00', '丽水', 'lishui', '0033005', '5', '6d846178376549ed878d11d109819f25', '丽水市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d5f621c6fab44d1eab1bdafd9d08e042', '绍兴', 'shaoxing', '0033008', '8', '6d846178376549ed878d11d109819f25', '绍兴市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d7443dcd45704a27981810fc32b93390', '潍坊', 'weifang', '0030314', '14', '10f46a521ea0471f8d71ee75ac3b5f3a', '潍坊市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d7c388a2ed58414a9bd4dfbedf6858b3', '顺义区', 'shunyiqu', '0030110', '10', '12a62a3e5bed44bba0412b7e6b733c93', '顺义区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d7e006b55b96491282e9c2e672d35a34', '克拉玛依', 'kelamayi', '0032808', '8', '2fabed91c6d94e698ed449165cd250ca', '克拉玛依', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d7e25a4040694008b4cb8aa322f77ad9', '张家界', 'zhangjiajie', '0031508', '8', 'c59f91630bef4289b71fcb2a48994582', '张家界市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d80455402bc44d2ca15e905913301fb2', '焦作', 'jiaozuo', '0031204', '4', '7336944efb4b40fcae9118fc9a970d2d', '焦作市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('d90a14bfbfe44a3e8d60bda8f8f362a6', '铜仁', 'tongren', '0030908', '8', '592f6fcf45a74524aa8ea853fc9761d5', '铜仁市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('da583c36f6754d498176755c93db8d7c', '遂宁', 'suining', '0032516', '16', 'd3538add7125404aba4b0007ef9fde50', '遂宁市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('db3b9c7116bc49e3a65fa641dd82155d', '红河', 'honghe', '0032907', '7', '510607a1836e4079b3103e14ec5864ed', '红河', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('db6336fcf27f4c00b37513ff0e368ae6', '长宁区', 'changningqu', '0030203', '3', 'f1ea30ddef1340609c35c88fb2919bee', '长宁区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('db77fd88654c4014a71d541171d2795b', '西安', 'xi\'an', '0032407', '7', '534850c72ceb4a57b7dc269da63c330a', '西安市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('dc022922169446549dfac0de221d2a4d', '雅安', 'ya\'an', '0032517', '17', 'd3538add7125404aba4b0007ef9fde50', '雅安市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('dc9b2098188f4b5c93aec5c9bbfb895d', '吕梁', 'lvliang', '0032306', '6', '023473e9e6204583a110531036357514', '吕梁市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('dcbcde16e75643f9b8dd4b6293c87dd5', '滨州', 'binzhou', '0030302', '2', '10f46a521ea0471f8d71ee75ac3b5f3a', '滨州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('dcd0ca1cde8f420dbfecbac4cf1506ee', '巴州', 'bazhou', '0032803', '3', '2fabed91c6d94e698ed449165cd250ca', '巴州', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('dcd445ef42c9484bbd14bacd02bebf37', '郑州', 'zhengzhou', '0031216', '16', '7336944efb4b40fcae9118fc9a970d2d', '郑州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('dcf99d941ca44b30915e16a4048d5004', '益阳', 'yiyang', '0031509', '9', 'c59f91630bef4289b71fcb2a48994582', '益阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('dee1fa3295ec42219815769d00fabe70', '迪庆', 'diqing', '0032906', '6', '510607a1836e4079b3103e14ec5864ed', '迪庆', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('e06c4a42478b4853827911b8adac6def', '庆阳', 'qingyang', '0030611', '11', '3283f1a77180495f9a0b192d0f9cdd35', '庆阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('e1204d4286834046886f26ae6af0722a', '湘潭', 'xiangtan', '0031503', '3', 'c59f91630bef4289b71fcb2a48994582', '湘潭市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('e15cb9bb072248628b7ec481b6337229', '六盘水', 'liupanshui', '0030904', '4', '592f6fcf45a74524aa8ea853fc9761d5', '六盘水市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('e1bbd9b635e140ee8fcf0dc06743519b', '廊坊', 'langfang', '0031106', '6', '75362368f22f4d60a810c2a45cced487', '廊坊市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('e384a07d11bf413eb83cd490939ca7a2', '綦江区', 'qijiangqu', '0033110', '10', '1c85fbd06cf840d093f3640aca1b6b2d', '綦江区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('e3ca4d54f3354ba5b17e1f93415ceb1a', '云浮', 'yunfu', '0030717', '17', '0dd1f40bcb9d46aeba015dc19645a5b9', '云浮市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('e4adf72e96ee4b7fa3528ee5ba4eb5cf', '梧州', 'wuzhou', '0030813', '13', 'c5f3d426c582410281f89f1451e1d854', '梧州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('e556d8e5e148406883e1a83d6595e406', '铜梁区', 'tongliangqu', '0033121', '21', '1c85fbd06cf840d093f3640aca1b6b2d', '铜梁区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('e7200f8c6dce4ea193bf33f55d9fd192', '池州', 'chizhou', '0030405', '5', '249999f296d14f95b8138a30bbb2c374', '池州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('e7de6c7b752040b2bd3175641d83d128', '哈尔滨', 'haerbin', '0031303', '3', 'b2d4133b5dbf4599ada940620d2ab250', '哈尔滨市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('e8311a6f0ab4495484fdf24902ee97cc', '新余', 'xinyu', '0031809', '9', 'cb3008cbd6ae4b5f8cebd2254ccb8603', '新余市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('e9a653c9850c46bc9e2e1916de643a52', '楚雄', 'chuxiong', '0032903', '3', '510607a1836e4079b3103e14ec5864ed', '楚雄', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ec0eed6293294d58aa56f6c381263288', '郴州', 'chenzhou', '0031510', '10', 'c59f91630bef4289b71fcb2a48994582', '郴州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ec107e60189346adb2b5749d6f6fe074', '德宏', 'dehong', '0032905', '5', '510607a1836e4079b3103e14ec5864ed', '德宏', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ec892838cf4944cc8b330216f02de1e6', '津南区', 'jinnanqu', '0032609', '9', '2c254799d3454f2cbc338ef5712548e9', '津南区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ec96c3771161459c99eb01124db7aa8a', '三门峡', 'sanmenxia', '0031211', '11', '7336944efb4b40fcae9118fc9a970d2d', '三门峡市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('eca8a2f4e2534f77b7bccf263139d8c7', '内江', 'neijiang', '0032513', '13', 'd3538add7125404aba4b0007ef9fde50', '内江市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ecb8f08c1408495bb31842c221d3edb4', '渝北区', 'yubeiqu', '0033112', '12', '1c85fbd06cf840d093f3640aca1b6b2d', '渝北区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ed5391a7608b4a61a24d95f2384f2131', '阜新', 'fuxin', '0031907', '7', 'b3366626c66c4b61881f09e1722e8495', '阜新市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ed5b3d39695f496d88c37f56508d6447', '仙桃', 'xiantao', '0031412', '12', '312b80775e104ba08c8244a042a658df', '仙桃市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ed97335f8b3d42fabfd89993bc68475d', '海南', 'hainan', '0032204', '4', '5a80e3435c0e4dc09bafceeadb38e5f0', '海南', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ede65c49ae624ef8900414298f79a438', '乌海', 'wuhai', '0032009', '9', 'c072c248c7ab47dda7bf24f5e577925c', '乌海市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f0570e2fe4644e32af5c5401e8c371ba', '盐城', 'yancheng', '0031711', '11', '577405ff648240959b3765c950598ab0', '盐城市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f0eb076930844fe18fdd8dcf5fc1f56e', '塔城', 'tacheng', '0032810', '10', '2fabed91c6d94e698ed449165cd250ca', '塔城', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f16ddc177870477685297a6abb157637', '朔州', 'shuozhou', '0032307', '7', '023473e9e6204583a110531036357514', '朔州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f1e2cdd9518c4ac2b5e1ea52985b9771', '果洛', 'guoluo', '0032201', '1', '5a80e3435c0e4dc09bafceeadb38e5f0', '果洛', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f1ea30ddef1340609c35c88fb2919bee', '上海', 'shanghai', '00302', '2', '1', '上海', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f260eee573dc48fca1572b228d280849', '版纳', 'banna', '0032901', '1', '510607a1836e4079b3103e14ec5864ed', '版纳', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f389ad0eb7884c4d91d4f31312bc8771', '萍乡', 'pingxiang', '0031807', '7', 'cb3008cbd6ae4b5f8cebd2254ccb8603', '萍乡市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f416737f56924f7cb642a75f57b1530a', '渭南', 'weinan', '0032406', '6', '534850c72ceb4a57b7dc269da63c330a', '渭南市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f498d09200ba48df9d6e281776eba4f8', '徐州', 'xuzhou', '0031710', '10', '577405ff648240959b3765c950598ab0', '徐州市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f4bdd6b8f0704479a6d051f52d62d693', '河池', 'hechi', '0030807', '7', 'c5f3d426c582410281f89f1451e1d854', '河池市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f4f2434769b842afbbf1791018b69800', '河北区', 'hebeiqu', '0032605', '5', '2c254799d3454f2cbc338ef5712548e9', '河北区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f57d2b8d983f43d5a035a596b690445b', '金山区', 'jinshanqu', '0030212', '12', 'f1ea30ddef1340609c35c88fb2919bee', '金山区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f6337bdeefa44b0db9f35fe2fe7d6d6f', '十堰', 'shiyan', '0031408', '8', '312b80775e104ba08c8244a042a658df', '十堰市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f661c388a73d478699a2c1c5909b45f1', '三亚', 'sanya', '0031002', '2', '2ba8e6d0fd944983aa19b781c6b53477', '三亚市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f6ff36eb35414a5dacf7ccc0c479d2ea', '宿迁', 'suqian', '0031707', '7', '577405ff648240959b3765c950598ab0', '宿迁市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f775a440cb004c63b0b3d3429b58a1e6', '衡水', 'hengshui', '0031105', '5', '75362368f22f4d60a810c2a45cced487', '衡水市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f845a1c0a62b45bfbf358688eec3680d', '巢湖', 'chaohu', '0030404', '4', '249999f296d14f95b8138a30bbb2c374', '巢湖市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f9565fe7c0a348ba949131645d20e8fa', '恩施', 'enshi', '0031402', '2', '312b80775e104ba08c8244a042a658df', '恩施市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f970bd1da8f94bfa92206fa94d595cbb', '锡林郭勒盟', 'xilinguolemeng', '0032011', '11', 'c072c248c7ab47dda7bf24f5e577925c', '锡林郭勒盟', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f9a9156f0e9e41438e823f93070248bd', '濮阳', 'puyang', '0031210', '10', '7336944efb4b40fcae9118fc9a970d2d', '濮阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('f9ceff59e02c4be3a4b20aa806c1ec0d', '呼和浩特', 'huhehaote', '0032006', '6', 'c072c248c7ab47dda7bf24f5e577925c', '呼和浩特市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('fa2ff170919e406d9d5547ff09d14d0d', '双鸭山', 'shuangyashan', '0031311', '11', 'b2d4133b5dbf4599ada940620d2ab250', '双鸭山市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('fa3446ef035546c09c1f27268b43b937', '南京', 'nanjing', '0031704', '4', '577405ff648240959b3765c950598ab0', '南京市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('fc70429d9b8146e0ac31ce38410e2cb7', '南阳', 'nanyang', '0031208', '8', '7336944efb4b40fcae9118fc9a970d2d', '南阳市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('fd06b72a41654fcfbfe2c3327ca4e9fc', '珠海', 'zhuhai', '0030721', '21', '0dd1f40bcb9d46aeba015dc19645a5b9', '珠海市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('fd1d83119c414e56b3a35052c9d6dd0f', '文山', 'wenshan', '0032914', '14', '510607a1836e4079b3103e14ec5864ed', '文山', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('fd2a0cad70c643528587d1ccde4c5530', '铜川', 'tongchuan', '0032405', '5', '534850c72ceb4a57b7dc269da63c330a', '铜川市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('fd79801a69ad4ec5857df82358c26368', '江北区', 'jiangbeiqu', '0033105', '5', '1c85fbd06cf840d093f3640aca1b6b2d', '江北区', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ff880943e156482ea50d1ece4ff233a6', '昌吉州', 'changjizhou', '0032805', '5', '2fabed91c6d94e698ed449165cd250ca', '昌吉州', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ffb2cc1e96fe485b94335589315ab38c', '临汾', 'linfen', '0032305', '5', '023473e9e6204583a110531036357514', '临汾市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ffd838f71da648319bfe4f176e0e209f', '晋中', 'jinzhong', '0032304', '4', '023473e9e6204583a110531036357514', '晋中市', '', null, null);
INSERT INTO `SYS_DICTIONARIES` VALUES ('ffeaa196501d4f35a486e42be17f2986', '枣庄', 'zaozhuang', '0030316', '16', '10f46a521ea0471f8d71ee75ac3b5f3a', '枣庄市', '', null, null);

-- ----------------------------
-- Table structure for `sys_fhbutton`
-- ----------------------------
DROP TABLE IF EXISTS `SYS_FHBUTTON`;
CREATE TABLE `SYS_FHBUTTON` (
  `FHBUTTON_ID` varchar(100) NOT NULL,
  `NAME` varchar(30) DEFAULT NULL COMMENT '名称',
  `SHIRO_KEY` varchar(50) DEFAULT NULL COMMENT '权限标识',
  `BZ` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`FHBUTTON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_fhbutton
-- ----------------------------
INSERT INTO `SYS_FHBUTTON` VALUES ('117825711d484e719a2a3bf1d0d45c2b', '发站内信', 'fhSms', '发站内信按钮');
INSERT INTO `SYS_FHBUTTON` VALUES ('3542adfbda73410c976e185ffe50ad06', '从系统导出到EXCEL', 'toExcel', '从系统导出到EXCEL');
INSERT INTO `SYS_FHBUTTON` VALUES ('c3d94fcfd3ee4a2a8f08bb1cd110bc5c', '发邮件', 'email', '发邮件按钮');
INSERT INTO `SYS_FHBUTTON` VALUES ('ed570ab55e4f4151a6654a4578aec787', '从EXCEL导入系统', 'fromExcel', '从EXCEL导入系统');

-- ----------------------------
-- Table structure for `SYS_FHLOG`
-- ----------------------------
DROP TABLE IF EXISTS `SYS_FHLOG`;
CREATE TABLE `SYS_FHLOG` (
  `FHLOG_ID` varchar(100) NOT NULL,
  `USERNAME` varchar(100) DEFAULT NULL COMMENT '用户名',
  `CZTIME` varchar(32) DEFAULT NULL COMMENT '操作时间',
  `CONTENT` longtext COMMENT '事件',
  PRIMARY KEY (`FHLOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_FHLOG
-- ----------------------------
INSERT INTO `SYS_FHLOG` VALUES ('0440031d9ae2496c81effe755aea867f', 'admin', '2019-05-26 15:55:45', '删除的菜单ID为:89');
INSERT INTO `SYS_FHLOG` VALUES ('05b9135939964091bce05d6e933f504d', 'admin', '2019-05-26 15:55:49', '删除的菜单ID为:88');
INSERT INTO `SYS_FHLOG` VALUES ('10daaf0c0d684ceab2d3e21c3eb14d43', 'admin', '2019-05-26 15:56:16', '退出系统');
INSERT INTO `SYS_FHLOG` VALUES ('14171d798a9e480184b54667805aafcf', 'admin', '2019-05-25 22:32:33', '退出系统');
INSERT INTO `SYS_FHLOG` VALUES ('1779bced07294702803c88206ba0ab01', 'admin', '2019-05-25 14:40:24', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('1ae1eb3482dc4f7299d131b7e6d126b5', 'admin', '2019-05-26 01:41:38', '退出系统');
INSERT INTO `SYS_FHLOG` VALUES ('1c7f05a639c746a6a3ce6fdd72f0e401', 'admin', '2019-05-25 15:09:28', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('2390e3f1da4e4311aba455d4610d9dda', 'admin', '2019-05-25 13:22:40', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('254a49bdd3814731856a53fa2fef370a', 'admin', '2019-05-25 22:22:25', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('297570d698ad446bb668ffa46b5ecd50', 'zhangsan', '2019-05-23 13:44:40', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('2c929b057fa54febb87ce74807fe4986', 'admin', '2019-05-26 01:43:16', '退出系统');
INSERT INTO `SYS_FHLOG` VALUES ('307a5ea75f484e7b8924c75c97ef397a', 'admin', '2019-05-23 13:38:51', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('3240d7e1ec664d7a8e4e925e716d4704', 'admin', '2019-05-26 15:55:59', '删除的菜单ID为:81');
INSERT INTO `SYS_FHLOG` VALUES ('359119edea5d4343840b83cf03cbad5b', 'zhangsan', '2019-05-26 01:34:51', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('37b8ff4d0cc24c25b340e846ae59b115', 'admin', '2019-05-26 15:56:06', '修改菜单:数据库管理');
INSERT INTO `SYS_FHLOG` VALUES ('3a543e67872347b7ac2de06d66af09d4', 'admin', '2019-05-25 16:20:58', '退出系统');
INSERT INTO `SYS_FHLOG` VALUES ('3d11032e6edb40a6824b5caaa876f605', 'admin', '2019-05-25 23:33:21', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('3d36e53f7bf94c74b130024c05776ed0', 'admin', '2019-05-26 15:55:34', '删除的菜单ID为:82');
INSERT INTO `SYS_FHLOG` VALUES ('436da5009ae3400db46c44fee58df3ab', 'zhangsan', '2019-05-26 01:44:25', '从个人资料中修改zhangsan的资料');
INSERT INTO `SYS_FHLOG` VALUES ('46d3b7130b7a4652b05899173702589c', 'admin', '2019-05-25 22:32:53', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('4d165a5d57be4d408d8b8ad0c723515e', 'admin', '2019-05-26 16:09:31', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('50836f9c269b439185355d1fcf849735', 'admin', '2019-05-26 15:55:29', '删除的菜单ID为:86');
INSERT INTO `SYS_FHLOG` VALUES ('58a3bb0d824f4733842e5be8f4bd3d03', 'admin', '2019-05-25 16:21:08', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('5fc4636a6c4b4550824e9b781c4bc92b', 'zhangsan', '2019-05-23 13:45:11', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('66578f4c636c47f0b6ed8ad592541102', 'admin', '2019-05-26 01:41:41', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('696c247aed7b40ae839620723da45ce4', 'admin', '2019-05-25 17:47:07', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('6ac18242d1aa44d981176784de1e474e', 'admin', '2019-05-26 15:55:24', '删除的菜单ID为:84');
INSERT INTO `SYS_FHLOG` VALUES ('6b30d9e8fb644da8a1060c02eaf2cc25', 'admin', '2019-05-26 02:20:30', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('6b91954181454dcdb8bffa7cedce4052', 'admin', '2019-05-26 15:56:19', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('6bf37afd09c94c9396edd5949819325f', 'admin', '2019-05-26 02:23:22', '退出系统');
INSERT INTO `SYS_FHLOG` VALUES ('72e4a0eedb174e47956b61e765cdc81c', 'admin', '2019-05-26 01:41:45', '退出系统');
INSERT INTO `SYS_FHLOG` VALUES ('7a10350a33254cc2a96814185cf2368e', 'admin', '2019-05-26 02:23:02', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('7f329aabafd045d2b8c98ae80b86a61f', 'admin', '2019-05-26 01:34:19', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('800b9ac4fcfc47d99b33bb4d08e2b0d4', 'admin', '2019-05-25 17:47:20', '从个人资料中修改admin的资料');
INSERT INTO `SYS_FHLOG` VALUES ('80219d62fe75426ca976e656272a9465', 'admin', '2019-05-26 20:00:33', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('8167be0ba8b54dd3906ac546450f6c57', 'admin', '2019-05-25 03:41:42', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('84b7225b0fc34c04a53bf2bb0efa96b8', 'admin', '2019-05-26 15:57:22', '删除按钮的ID为：1af9cef37b1f4cc79abacb52c25a3dfa');
INSERT INTO `SYS_FHLOG` VALUES ('86e88806c4804ecda3d87f1d96a96751', 'admin', '2019-05-26 15:57:33', '删除按钮的ID为：7b9756f1455d418396d14bf5a1f8ed09');
INSERT INTO `SYS_FHLOG` VALUES ('8c1210696cbb4fdfbf6f79085498611c', 'admin', '2019-05-26 15:57:28', '删除按钮的ID为：e2a0d76a25bd467aa29aaadf3a8def18');
INSERT INTO `SYS_FHLOG` VALUES ('91f5e6ad33e547bba9b14ef0d5012424', 'admin', '2019-05-25 13:10:32', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('a2b5f3382a74451b8c9ed897031822af', 'admin', '2019-05-23 13:45:08', '退出系统');
INSERT INTO `SYS_FHLOG` VALUES ('a5b44f900831489aaed9f9c96a84dbcb', 'admin', '2019-05-26 15:57:37', '删除按钮的ID为：688f6db8b226468e82e0f2c40d377fd9');
INSERT INTO `SYS_FHLOG` VALUES ('aaba8a9dc14c488db876fd717ed66092', 'admin', '2019-05-26 01:41:06', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('b63d9b6cc1574339a429dbd5e8052741', 'admin', '2019-05-26 15:55:54', '删除的菜单ID为:87');
INSERT INTO `SYS_FHLOG` VALUES ('b6d0ff554cd14909ba16236167adec33', 'admin', '2019-05-26 16:09:54', '退出系统');
INSERT INTO `SYS_FHLOG` VALUES ('b74585430c084ebb9416f83f040e0f27', 'admin', '2019-05-23 13:28:39', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('b95e42eacbda4c4ea960fd31d75e1b1b', 'admin', '2019-05-26 15:53:36', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('ba8fdf642f964d1680328a8bd86e724f', 'admin', '2019-05-26 16:02:54', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('bf59d7dde1d04bf89df60065686efdb8', 'admin', '2019-05-26 15:55:18', '删除的菜单ID为:83');
INSERT INTO `SYS_FHLOG` VALUES ('c3d4c7b83c144bd79c78b71c83ac27d7', 'admin', '2019-05-26 16:08:48', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('c632f095e47b49b38e3b2dbe7d276b33', 'admin', '2019-05-25 14:11:58', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('c96535945ba742949ccb5706e7d66b03', 'admin', '2019-05-26 02:23:37', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('ca500e4602344d3a94ba52b0da7acfea', 'admin', '2019-05-26 16:09:24', '退出系统');
INSERT INTO `SYS_FHLOG` VALUES ('d0be31a447ec427f88944d4e5d18efb8', 'admin', '2019-05-26 15:55:40', '删除的菜单ID为:91');
INSERT INTO `SYS_FHLOG` VALUES ('d77e88b5fc8e426281416e4b01a2acc1', 'admin', '2019-05-26 15:55:38', '删除的菜单ID为:90');
INSERT INTO `SYS_FHLOG` VALUES ('d783b394584c41bba6e7fe448fafd8ac', 'admin', '2019-05-25 16:20:52', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('d8b38912f6544311a5a02b7b33e9ddd8', 'admin', '2019-05-25 13:14:43', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('e3a079de77514aa8a3828f4f617b9ec7', 'zhangsan', '2019-05-26 02:23:30', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('e44fd72d431841108fb8333f5447cfb3', 'zhangsan', '2019-05-23 13:42:52', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('e47a611d4a9844b080ba718797e61d2b', 'admin', '2019-05-26 15:55:27', '删除的菜单ID为:85');
INSERT INTO `SYS_FHLOG` VALUES ('f17c0e654d1746068f85e9b47cd4a178', 'admin', '2019-05-26 01:42:38', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('f6d07d172ecc41689074f3513d96679e', 'admin', '2019-05-23 13:43:11', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('f71f657241514a0b8f457faeb1d48d4e', 'admin', '2019-05-26 01:43:25', '成功登录系统');
INSERT INTO `SYS_FHLOG` VALUES ('f873fc525d9945a58e94276fb303f97a', 'admin', '2019-05-26 01:40:37', '退出系统');

-- ----------------------------
-- Table structure for `sys_fhsms`
-- ----------------------------
DROP TABLE IF EXISTS `SYS_FHSMS`;
CREATE TABLE `SYS_FHSMS` (
  `FHSMS_ID` varchar(100) NOT NULL,
  `CONTENT` varchar(1000) DEFAULT NULL COMMENT '内容',
  `TYPE` varchar(5) DEFAULT NULL COMMENT '类型',
  `TO_USERNAME` varchar(255) DEFAULT NULL COMMENT '收信人',
  `FROM_USERNAME` varchar(255) DEFAULT NULL COMMENT '发信人',
  `SEND_TIME` varchar(100) DEFAULT NULL COMMENT '发信时间',
  `STATUS` varchar(5) DEFAULT NULL COMMENT '状态',
  `SANME_ID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`FHSMS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_fhsms
-- ----------------------------

-- ----------------------------
-- Table structure for `SYS_MENU`
-- ----------------------------
DROP TABLE IF EXISTS `SYS_MENU`;
CREATE TABLE `SYS_MENU` (
  `MENU_ID` int(11) NOT NULL,
  `MENU_NAME` varchar(255) DEFAULT NULL,
  `MENU_URL` varchar(255) DEFAULT NULL,
  `PARENT_ID` varchar(100) DEFAULT NULL,
  `MENU_ORDER` varchar(100) DEFAULT NULL,
  `MENU_ICON` varchar(60) DEFAULT NULL,
  `MENU_TYPE` varchar(10) DEFAULT NULL,
  `MENU_STATE` int(1) DEFAULT NULL,
  `SHIRO_KEY` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`MENU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of SYS_MENU
-- ----------------------------
INSERT INTO `SYS_MENU` VALUES ('1', '系统管理', '#', '0', '1', 'feather icon-monitor', '2', '1', null);
INSERT INTO `SYS_MENU` VALUES ('2', '权限管理', '#', '1', '1', 'zmdi zmdi-key zmdi-hc-fw', '1', '1', null);
INSERT INTO `SYS_MENU` VALUES ('3', '日志管理', '../fhlog/fhlog_list.html', '1', '6', 'zmdi zmdi-keyboard-hide zmdi-hc-fw', '1', '1', 'fhlog:list');
INSERT INTO `SYS_MENU` VALUES ('4', '性能监控', '../../tools/druid/index.html', '5', '2', 'zmdi zmdi-devices zmdi-hc-fw', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('5', '系统工具', '#', '0', '4', 'feather icon-grid', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('6', '角色(基础权限)', '../role/role_list.html', '2', '1', 'zmdi zmdi-account-calendar zmdi-hc-fw', '1', '1', 'role:list');
INSERT INTO `SYS_MENU` VALUES ('7', '按钮权限', '../buttonrights/buttonrights_list.html', '2', '2', 'zmdi zmdi-shield-security zmdi-hc-fw', '1', '1', 'buttonrights:list');
INSERT INTO `SYS_MENU` VALUES ('8', '菜单管理', '../menu/menu_ztree.html', '1', '3', 'zmdi zmdi-collection-text zmdi-hc-fw', '1', '1', 'menu:list');
INSERT INTO `SYS_MENU` VALUES ('9', '按钮管理', '../fhbutton/fhbutton_list.html', '1', '2', 'zmdi zmdi-picture-in-picture zmdi-hc-fw', '1', '1', 'fhbutton:list');
INSERT INTO `SYS_MENU` VALUES ('10', '用户管理', '#', '0', '2', 'feather icon-users', '2', '1', null);
INSERT INTO `SYS_MENU` VALUES ('11', '系统用户', '../user/user_list.html', '10', '1', 'zmdi zmdi-accounts zmdi-hc-fw', '1', '1', 'user:list');
INSERT INTO `SYS_MENU` VALUES ('12', '数据字典', '../dictionaries/dictionaries_ztree.html', '1', '4', 'zmdi zmdi-assignment zmdi-hc-fw', '1', '1', 'dictionaries:list');
INSERT INTO `SYS_MENU` VALUES ('13', '代码生成器', '#', '5', '1', 'zmdi zmdi-keyboard zmdi-hc-fw', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('14', '正向生成', '../../tools/createcode/createcode_list.html', '13', '1', 'zmdi zmdi-square-right zmdi-hc-fw', '1', '1', 'createCode:list');
INSERT INTO `SYS_MENU` VALUES ('15', '反向生成', '../../tools/createcode/recreatecode_list.html', '13', '2', 'zmdi zmdi-rotate-left zmdi-hc-fw', '1', '1', 'recreateCode:list');
INSERT INTO `SYS_MENU` VALUES ('16', '模版管理', '../../tools/codeeditor/codeeditor_edit_1.html?type=createOneCode&ffile=controllerTemplate', '13', '3', 'zmdi zmdi-assignment zmdi-hc-fw', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('17', '单表模版', '../../tools/codeeditor/codeeditor_edit_1.html?type=createOneCode&ffile=controllerTemplate', '16', '1', 'zmdi zmdi-folder-outline zmdi-hc-fw', '1', '1', 'codeeditor:list');
INSERT INTO `SYS_MENU` VALUES ('18', '主表模版', '../../tools/codeeditor/codeeditor_edit_1.html?type=createFaCode&ffile=controllerTemplate', '16', '2', 'zmdi zmdi-folder-outline zmdi-hc-fw', '1', '1', 'codeeditor:list');
INSERT INTO `SYS_MENU` VALUES ('19', '明细表模版', '../../tools/codeeditor/codeeditor_edit_1.html?type=createSoCode&ffile=controllerTemplate', '16', '3', 'zmdi zmdi-folder-outline zmdi-hc-fw', '1', '1', 'codeeditor:list');
INSERT INTO `SYS_MENU` VALUES ('20', '树形表模版', '../../tools/codeeditor/codeeditor_edit_1.html?type=createTreeCode&ffile=controllerTemplate', '16', '4', 'zmdi zmdi-folder-outline zmdi-hc-fw', '1', '1', 'codeeditor:list');
INSERT INTO `SYS_MENU` VALUES ('21', '处理类', '../../tools/codeeditor/codeeditor_edit_1.html?type=createOneCode&ffile=controllerTemplate', '17', '1', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('22', 'mapper层', '../../tools/codeeditor/codeeditor_edit_1.html?type=createOneCode&ffile=mapperTemplate', '17', '2', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('23', 'service层', '../../tools/codeeditor/codeeditor_edit_1.html?type=createOneCode&ffile=serviceTemplate', '17', '3', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('24', 'serviceI实现类', '../../tools/codeeditor/codeeditor_edit_1.html?type=createOneCode&ffile=serviceImplTemplate', '17', '4', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('25', 'mybatis_xml_mysql', '../../tools/codeeditor/codeeditor_edit_2.html?type=createOneCode&ffile=xml_MysqlTemplate', '17', '5', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('26', 'mybatis_xml_oracle', '../../tools/codeeditor/codeeditor_edit_2.html?type=createOneCode&ffile=xml_OracleTemplate', '17', '6', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('27', 'mybatis_xml_sqlserver', '../../tools/codeeditor/codeeditor_edit_2.html?type=createOneCode&ffile=xml_SqlserverTemplate', '17', '7', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('28', 'mysql_sql_脚本', '../../tools/codeeditor/codeeditor_edit_1.html?type=createOneCode&ffile=mysql_SQL_Template', '17', '8', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('29', 'oracle_sql_脚本', '../../tools/codeeditor/codeeditor_edit_1.html?type=createOneCode&ffile=oracle_SQL_Template', '17', '9', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('30', 'sqlserver_sql_脚本', '../../tools/codeeditor/codeeditor_edit_1.html?type=createOneCode&ffile=sqlserver_SQL_Template', '17', '10', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('31', 'html_列表页面', '../../tools/codeeditor/codeeditor_edit_2.html?type=createOneCode&ffile=html_list_Template', '17', '11', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('32', 'html_编辑页面', '../../tools/codeeditor/codeeditor_edit_2.html?type=createOneCode&ffile=html_edit_Template', '17', '12', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('33', '处理类', '../../tools/codeeditor/codeeditor_edit_1.html?type=createFaCode&ffile=controllerTemplate', '18', '1', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('34', 'mapper层', '../../tools/codeeditor/codeeditor_edit_1.html?type=createFaCode&ffile=mapperTemplate', '18', '2', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('35', 'service层 ', '../../tools/codeeditor/codeeditor_edit_1.html?type=createFaCode&ffile=serviceTemplate', '18', '3', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('36', 'serviceI实现类', '../../tools/codeeditor/codeeditor_edit_1.html?type=createFaCode&ffile=serviceImplTemplate', '18', '4', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('37', 'mybatis_xml_mysql', '../../tools/codeeditor/codeeditor_edit_2.html?type=createFaCode&ffile=xml_MysqlTemplate', '18', '5', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('38', 'mybatis_xml_oracle', '../../tools/codeeditor/codeeditor_edit_2.html?type=createFaCode&ffile=xml_OracleTemplate', '18', '6', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('39', 'mybatis_xml_sqlserver', '../../tools/codeeditor/codeeditor_edit_2.html?type=createFaCode&ffile=xml_SqlserverTemplate', '18', '7', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('40', 'mysql_sql_脚本', '../../tools/codeeditor/codeeditor_edit_1.html?type=createFaCode&ffile=mysql_SQL_Template', '18', '8', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('41', 'oracle_sql_脚本', '../../tools/codeeditor/codeeditor_edit_1.html?type=createFaCode&ffile=oracle_SQL_Template', '18', '9', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('42', 'sqlserver_sql_脚本', '../../tools/codeeditor/codeeditor_edit_1.html?type=createFaCode&ffile=sqlserver_SQL_Template', '18', '10', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('43', 'html_列表页面', '../../tools/codeeditor/codeeditor_edit_2.html?type=createFaCode&ffile=html_list_Template', '18', '11', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('44', 'html_编辑页面', '../../tools/codeeditor/codeeditor_edit_2.html?type=createFaCode&ffile=html_edit_Template', '18', '12', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('45', '处理类', '../../tools/codeeditor/codeeditor_edit_1.html?type=createSoCode&ffile=controllerTemplate', '19', '1', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('46', 'mapper层', '../../tools/codeeditor/codeeditor_edit_1.html?type=createSoCode&ffile=mapperTemplate', '19', '2', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('47', 'service层', '../../tools/codeeditor/codeeditor_edit_1.html?type=createSoCode&ffile=serviceTemplate', '19', '3', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('48', 'serviceI实现类', '../../tools/codeeditor/codeeditor_edit_1.html?type=createSoCode&ffile=serviceImplTemplate', '19', '4', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('49', 'mybatis_xml_mysql', '../../tools/codeeditor/codeeditor_edit_2.html?type=createSoCode&ffile=xml_MysqlTemplate', '19', '5', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('50', 'mybatis_xml_oracle', '../../tools/codeeditor/codeeditor_edit_2.html?type=createSoCode&ffile=xml_OracleTemplate', '19', '6', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('51', 'mybatis_xml_sqlserver', '../../tools/codeeditor/codeeditor_edit_2.html?type=createSoCode&ffile=xml_SqlserverTemplate', '19', '7', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('52', 'mysql_sql_脚本', '../../tools/codeeditor/codeeditor_edit_1.html?type=createSoCode&ffile=mysql_SQL_Template', '19', '8', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('53', 'oracle_sql_脚本', '../../tools/codeeditor/codeeditor_edit_1.html?type=createSoCode&ffile=oracle_SQL_Template', '19', '9', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('54', 'sqlserver_sql_脚本', '../../tools/codeeditor/codeeditor_edit_1.html?type=createSoCode&ffile=sqlserver_SQL_Template', '19', '10', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('55', 'html_列表页面', '../../tools/codeeditor/codeeditor_edit_2.html?type=createSoCode&ffile=html_list_Template', '19', '11', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('56', 'html_编辑页面', '../../tools/codeeditor/codeeditor_edit_2.html?type=createSoCode&ffile=html_edit_Template', '19', '12', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('57', '处理类', '../../tools/codeeditor/codeeditor_edit_1.html?type=createTreeCode&ffile=controllerTemplate', '20', '1', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('58', '实体类', '../../tools/codeeditor/codeeditor_edit_1.html?type=createTreeCode&ffile=entityTemplate', '20', '2', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('59', 'mapper层', '../../tools/codeeditor/codeeditor_edit_1.html?type=createTreeCode&ffile=mapperTemplate', '20', '3', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('60', 'service层', '../../tools/codeeditor/codeeditor_edit_1.html?type=createTreeCode&ffile=serviceTemplate', '20', '4', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('61', 'serviceI实现类', '../../tools/codeeditor/codeeditor_edit_1.html?type=createTreeCode&ffile=serviceImplTemplate', '20', '5', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('62', 'mybatis_xml_mysql', '../../tools/codeeditor/codeeditor_edit_2.html?type=createTreeCode&ffile=xml_MysqlTemplate', '20', '6', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('63', 'mybatis_xml_oracle', '../../tools/codeeditor/codeeditor_edit_2.html?type=createTreeCode&ffile=xml_OracleTemplate', '20', '7', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('64', 'mybatis_xml_sqlserver', '../../tools/codeeditor/codeeditor_edit_2.html?type=createTreeCode&ffile=xml_SqlserverTemplate', '20', '8', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('65', 'mysql_sql_脚本', '../../tools/codeeditor/codeeditor_edit_1.html?type=createTreeCode&ffile=mysql_SQL_Template', '20', '9', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('66', 'oracle_sql_脚本', '../../tools/codeeditor/codeeditor_edit_1.html?type=createTreeCode&ffile=oracle_SQL_Template', '20', '10', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('67', 'sqlserver_sql_脚本', '../../tools/codeeditor/codeeditor_edit_1.html?type=createTreeCode&ffile=sqlserver_SQL_Template', '20', '11', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('68', 'tree_树形页面', '../../tools/codeeditor/codeeditor_edit_2.html?type=createTreeCode&ffile=html_tree_Template', '20', '12', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('69', 'html_列表页面', '../../tools/codeeditor/codeeditor_edit_2.html?type=createTreeCode&ffile=html_list_Template', '20', '13', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('70', 'html_编辑页面', '../../tools/codeeditor/codeeditor_edit_2.html?type=createTreeCode&ffile=html_edit_Template', '20', '14', '', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('71', '在线管理', '../online/online.html', '1', '6', 'zmdi zmdi-globe zmdi-hc-fw', '1', '1', 'online:list');
INSERT INTO `SYS_MENU` VALUES ('72', '我的通讯', '#', '0', '3', 'feather icon-phone-call', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('73', '好友管理', '../../fhim/friends/friends_list.html', '72', '1', 'zmdi zmdi-account zmdi-hc-fw', '1', '1', 'friends:list');
INSERT INTO `SYS_MENU` VALUES ('74', '好友分组', '../../fhim/fgroup/fgroup_list.html', '72', '2', 'zmdi zmdi-accounts-alt zmdi-hc-fw', '1', '1', 'fgroup:list');
INSERT INTO `SYS_MENU` VALUES ('75', '我的群组', '../../fhim/qgroup/qgroup_list.html', '72', '3', 'zmdi zmdi-male-female zmdi-hc-fw', '1', '1', 'qgroup:list');
INSERT INTO `SYS_MENU` VALUES ('76', '数据库管理', '#', '0', '5', 'mdi mdi-database', '1', '1', '(无)');
INSERT INTO `SYS_MENU` VALUES ('77', '数据库备份', '../../fhdb/brdb/table_list.html', '76', '1', 'zmdi zmdi-assignment-returned zmdi-hc-fw', '1', '1', 'brdb:listAllTable');
INSERT INTO `SYS_MENU` VALUES ('78', '备份定时器 ', '../../fhdb/backup/backup_list.html', '76', '2', 'zmdi zmdi-alarm zmdi-hc-fw', '1', '1', 'timingbackup:list');
INSERT INTO `SYS_MENU` VALUES ('79', '数据库还原', '../../fhdb/brdb/brdb_list.html', '76', '3', 'zmdi zmdi-assignment-return zmdi-hc-fw', '1', '1', 'brdb:list');
INSERT INTO `SYS_MENU` VALUES ('80', 'SQL编辑器', '../../fhdb/sqledit/sql_edit.html', '76', '4', 'zmdi zmdi-receipt zmdi-hc-fw', '1', '1', 'sqledit:view');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `SYS_ROLE`;
CREATE TABLE `SYS_ROLE` (
  `ROLE_ID` varchar(100) NOT NULL,
  `ROLE_NAME` varchar(100) DEFAULT NULL,
  `RIGHTS` varchar(255) DEFAULT NULL,
  `PARENT_ID` varchar(100) DEFAULT NULL,
  `ADD_QX` varchar(255) DEFAULT NULL,
  `DEL_QX` varchar(255) DEFAULT NULL,
  `EDIT_QX` varchar(255) DEFAULT NULL,
  `CHA_QX` varchar(255) DEFAULT NULL,
  `RNUMBER` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `SYS_ROLE` VALUES ('1', '系统管理组', '9903520314283042199192993790', '0', '1', '1', '1', '1', 'R20170000000001');
INSERT INTO `SYS_ROLE` VALUES ('147d873ec4154673a15ac7c093b68657', '备用组', '', '0', '0', '0', '0', '0', 'R20190219170377');
INSERT INTO `SYS_ROLE` VALUES ('e295424edf854dac8329dee5a6b7f016', '部门经理', '4799506339367320868261724160', '1', '4799506339367320868261724160', '4799506339367320868261724160', '4799506339367320868261724160', '4799506339367320868261724160', 'R20171231556774');
INSERT INTO `SYS_ROLE` VALUES ('ebdb884104f24671ad8d30440242adad', '员工', '4799506339367320868261724160', '1', '4799506339367320868261724160', '4799506339367320868261724160', '4799506339367320868261724160', '4799506339367320868261724160', 'R20171231102049');
INSERT INTO `SYS_ROLE` VALUES ('f1cb1a689f2a4b57bcf8761b350c0ffc', '总经理', '4799506339367320868261724160', '1', '4799506339367320868261724160', '4799506339367320868261724160', '4799506339367320868261724160', '4799506339367320868261724160', 'R20171231726481');
INSERT INTO `SYS_ROLE` VALUES ('fhadminzhuche', '注册用户', '0', '1', '', '', '0', '0', 'R20171231000000');

-- ----------------------------
-- Table structure for `sys_role_fhbutton`
-- ----------------------------
DROP TABLE IF EXISTS `SYS_ROLE_FHBUTTON`;
CREATE TABLE `SYS_ROLE_FHBUTTON` (
  `RB_ID` varchar(100) NOT NULL,
  `ROLE_ID` varchar(100) DEFAULT NULL,
  `BUTTON_ID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`RB_ID`),
  KEY `角色表外键` (`ROLE_ID`) USING BTREE,
  KEY `fbutton` (`BUTTON_ID`),
  CONSTRAINT `sys_role_fhbutton_ibfk_1` FOREIGN KEY (`BUTTON_ID`) REFERENCES `sys_fhbutton` (`FHBUTTON_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_role_fhbutton_ibfk_2` FOREIGN KEY (`ROLE_ID`) REFERENCES `sys_role` (`ROLE_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_fhbutton
-- ----------------------------
INSERT INTO `SYS_ROLE_FHBUTTON` VALUES ('0188077fa0d54e99a37e530011a314eb', 'fhadminzhuche', '117825711d484e719a2a3bf1d0d45c2b');
INSERT INTO `SYS_ROLE_FHBUTTON` VALUES ('2e941c79fdd34387b292e27444ba081f', 'f1cb1a689f2a4b57bcf8761b350c0ffc', '117825711d484e719a2a3bf1d0d45c2b');
INSERT INTO `SYS_ROLE_FHBUTTON` VALUES ('34edcd073e5e480c824166e5dfd9d2eb', 'e295424edf854dac8329dee5a6b7f016', 'c3d94fcfd3ee4a2a8f08bb1cd110bc5c');
INSERT INTO `SYS_ROLE_FHBUTTON` VALUES ('45caf625a8bd4f7d92233d6be53d1e67', 'ebdb884104f24671ad8d30440242adad', '117825711d484e719a2a3bf1d0d45c2b');
INSERT INTO `SYS_ROLE_FHBUTTON` VALUES ('53787c5736cc4b4da21e51f9f9c7b042', 'ebdb884104f24671ad8d30440242adad', 'c3d94fcfd3ee4a2a8f08bb1cd110bc5c');
INSERT INTO `SYS_ROLE_FHBUTTON` VALUES ('7d7400610af745af91a78b5320cf0078', 'e295424edf854dac8329dee5a6b7f016', '117825711d484e719a2a3bf1d0d45c2b');
INSERT INTO `SYS_ROLE_FHBUTTON` VALUES ('cf38f7e5708140a0923999442665faef', 'f1cb1a689f2a4b57bcf8761b350c0ffc', 'c3d94fcfd3ee4a2a8f08bb1cd110bc5c');

-- ----------------------------
-- Table structure for `sys_ueditor`
-- ----------------------------
DROP TABLE IF EXISTS `SYS_UEDITOR`;
CREATE TABLE `SYS_UEDITOR` (
  `UEDITOR_ID` varchar(100) NOT NULL,
  `USER_ID` varchar(100) DEFAULT NULL COMMENT '用户ID',
  `USERNAME` varchar(100) DEFAULT NULL COMMENT '用户名',
  `CONTENT` longtext COMMENT '文本内容',
  `CONTENT2` longtext COMMENT '类型',
  `TYPE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`UEDITOR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_ueditor
-- ----------------------------
INSERT INTO `SYS_UEDITOR` VALUES ('5aa7cf78054045e99cfb887040522cd0', '1', 'admin', '', '', 'fhsms');

-- ----------------------------
-- Table structure for `SYS_USER`
-- ----------------------------
DROP TABLE IF EXISTS `SYS_USER`;
CREATE TABLE `SYS_USER` (
  `USER_ID` varchar(100) NOT NULL COMMENT '用户ID',
  `USERNAME` varchar(255) DEFAULT NULL COMMENT '用户名',
  `PASSWORD` varchar(255) DEFAULT NULL COMMENT '密码',
  `NAME` varchar(255) DEFAULT NULL COMMENT '姓名',
  `ROLE_ID` varchar(100) DEFAULT NULL COMMENT '角色ID',
  `LAST_LOGIN` varchar(255) DEFAULT NULL COMMENT '最近登录时间',
  `IP` varchar(100) DEFAULT NULL COMMENT 'IP',
  `STATUS` varchar(32) DEFAULT NULL COMMENT '状态',
  `BZ` varchar(255) DEFAULT NULL COMMENT '备注',
  `SKIN` varchar(500) DEFAULT NULL COMMENT '皮肤',
  `EMAIL` varchar(32) DEFAULT NULL COMMENT '邮箱',
  `NUMBER` varchar(100) DEFAULT NULL COMMENT '编码',
  `PHONE` varchar(32) DEFAULT NULL COMMENT '电话',
  `ROLE_IDS` varchar(2000) DEFAULT NULL COMMENT '副职角色ID组',
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `SYS_USER` VALUES ('1', 'admin', 'de41b7fb99201d8334c23c014db35ecd92df81bc', '系统管理员', '1', '2019-05-26 20:22:19', '127.0.0.1', '0', 'admin', 'pcoded-navbar navbar-image-1,navbar pcoded-header navbar-expand-lg navbar-light header-dark,', 'QQ313596790@qq.com', '001', '18788888888', '');
INSERT INTO `SYS_USER` VALUES ('3a6c44458b6243fbb9c2bcf30d422e33', 'lisi', '2612ade71c1e48cd7150b5f4df152faa699cedfe', '李四', 'e295424edf854dac8329dee5a6b7f016', '2019-05-23 03:55:20', '192.168.0.8', '0', '李四', 'pcoded-navbar navbar-image-3,navbar pcoded-header navbar-expand-lg navbar-light header-dark,', '123456@qq.com', 'z002', '18788885858', 'e295424edf854dac8329dee5a6b7f016');
INSERT INTO `SYS_USER` VALUES ('62c0aafd97704c3a85ef0fca3048045d', 'zhangsan', '5ee5d458d02fde6170b9c3ebfe06af85dacd131d', '张三', 'ebdb884104f24671ad8d30440242adad', '2019-05-26 02:23:30', '127.0.0.1', '0', '张三', 'pcoded-navbar navbar-image-3,navbar pcoded-header navbar-expand-lg navbar-light header-dark,', '187658888888@qq.con', 'z001', '18765888888', '');
INSERT INTO `SYS_USER` VALUES ('d7b34acd89d6441da59787477425e91f', 'wangwu', '99162695b36d0b7d54bab64468bdbb89c6ad45c5', '王五', 'f1cb1a689f2a4b57bcf8761b350c0ffc', '2019-05-16 02:20:37', '127.0.0.1', '0', '王五', 'pcoded-navbar navbar-image-3,navbar pcoded-header navbar-expand-lg navbar-light header-dark,', '5555555@qq.com', 'z003', '18756666666', '');

-- ----------------------------
-- Table structure for `SYS_USERPHOTO`
-- ----------------------------
DROP TABLE IF EXISTS `SYS_USERPHOTO`;
CREATE TABLE `SYS_USERPHOTO` (
  `USERPHOTO_ID` varchar(100) NOT NULL,
  `USERNAME` varchar(30) DEFAULT NULL COMMENT '用户名',
  `PHOTO0` varchar(255) DEFAULT NULL COMMENT '原图',
  `PHOTO1` varchar(100) DEFAULT NULL COMMENT '头像1',
  `PHOTO2` varchar(100) DEFAULT NULL COMMENT '头像2',
  `PHOTO3` varchar(100) DEFAULT NULL COMMENT '头像3',
  PRIMARY KEY (`USERPHOTO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_userphoto
-- ----------------------------
